package time_table.domain.base;

import time_table.converter.sheet.ExcelSheet;

import java.io.IOException;
import java.util.List;

public interface RowsImporter<T extends ExcelSheet, L> {

    List<L> importSheet(T excelSheet) throws IOException;

}
