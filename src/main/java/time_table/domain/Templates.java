package time_table.domain;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class Templates {

    public static final Template SCHEDULE_TEMPLATE = new Template("schedule-template", "Teaching allocation document");

    private static final Set<Template> SET_SCHEDULE_TEMPLATE = Set.of(
            SCHEDULE_TEMPLATE
    );

    public final static Set<Template> SUPPORTED_TEMPLATES = Stream.of(SET_SCHEDULE_TEMPLATE)
            .flatMap(Set::stream)
            .collect(Collectors.toSet());

    public static boolean isSupported(String template) {
        return SUPPORTED_TEMPLATES.stream()
                .map(Template::name)
                .anyMatch(t -> t.equalsIgnoreCase(template));
    }

    public static boolean isNotSupported(String template) {
        return !isSupported(template);
    }

}
