package time_table.domain.data_arrangements;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import time_table.domain.dto.SubGroupTableDto;

import java.io.Serializable;
import java.util.List;

/**
 * Model of the generated timetable for subgroups with their group
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GroupWithSubGroupsTable implements Serializable {

    private String group;

    private List<SubGroupTableDto> subGroupsTable;

}
