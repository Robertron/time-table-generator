package time_table.domain.data_arrangements;

import lombok.Data;
import time_table.domain.dto.GroupStudyDto;

import java.io.Serializable;
import java.util.List;

@Data
public class GroupTable implements Serializable {

    private String group;

    private List<GroupStudyDto> groupStudyDtoList;

}
