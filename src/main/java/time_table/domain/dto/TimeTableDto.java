package time_table.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * Model of the generated timetable
 */
@Schema(description = "Generated time table schedule")

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TimeTableDto implements Serializable {

    @Schema(description = "Student group", example = "BS - Year 1")
    @JsonProperty(value = "group")
    private String group;

    @Schema(description = "List of study subjects and their characteristics for a group")
    @JsonProperty(value = "study")
    private List<GroupStudyDto> groupStudyDtoList;

    @Schema(description = "Subgroup table schedule for a given student group")
    @JsonProperty(value = "subGroupsTable")
    private List<SubGroupTableDto> subGroupsTable;
}
