package time_table.domain.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import time_table.util.LocalTimeSerializer;

import java.time.LocalTime;

/**
 * Model of the study period for a given subgroup in a given day
 */
@Schema(description = "Study period in a given day for a given subject")

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SubGroupPeriodDto {

    @JsonIgnore
    @Schema(description = "Study period in a given day", example = "Period 1")
    @JsonProperty(value = "period")
    private String period;

    @Schema(description = "Start of a study period in a given day", example = "9:00")
    @JsonProperty(value = "startTime")
    @JsonSerialize(using = LocalTimeSerializer.class)//https://stackoverflow.com/a/27952473/11322439
    @JsonDeserialize(using = LocalTimeSerializer.LocalTimeDeserializer.class)
    private LocalTime startTime;

    @Schema(description = "End of a study period in a given day", example = "10:30")
    @JsonProperty(value = "endTime")
    @JsonSerialize(using = LocalTimeSerializer.class)
    @JsonDeserialize(using = LocalTimeSerializer.LocalTimeDeserializer.class)
    private LocalTime endTime;

    @Schema(description = "Lab being taught", example = "Theoretical Computer Science - (LAB)")
    @JsonProperty(value = "lab")
    private String subjectLab;

    @Schema(description = "Number of students in one lab", example = "45")
    @JsonProperty(value = "numberOfStudents")
    private Integer numberOfStudents;

    @Schema(description = "Assistant", example = "Assistant 1")
    @JsonProperty(value = "assistant")
    private String assistantName;

    @Schema(description = "Study room or auditorium", example = "108")
    @JsonProperty(value = "room")
    private Integer room;
}
