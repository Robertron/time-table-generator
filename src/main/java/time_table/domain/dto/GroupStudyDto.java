package time_table.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Model of the study schedule that reflects days of study and the periods in each day
 */
@Schema(description = "Study schedule for a given group")

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GroupStudyDto {

    @Schema(description = "Study day", example = "MONDAY")
    @JsonProperty(value = "day")
    private String day;

    @Schema(description = "List of study periods for a group in a given study day")
    @JsonProperty(value = "periods")
    private List<GroupPeriodDto> groupPeriodDtos;
}
