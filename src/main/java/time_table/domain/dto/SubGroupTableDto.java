package time_table.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * Model of the generated timetable
 */
@Schema(description = "Generated time table schedule for subgroups")

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SubGroupTableDto implements Serializable {

    @Schema(description = "Student subgroup", example = "B22-CS-05")
    @JsonProperty(value = "subgroup")
    private String subGroup;

    @Schema(description = "List of study labs for a subgroup and their characteristics")
    @JsonProperty(value = "study")
    private List<SubGroupStudyDto> subGroupStudyDtoList;
}
