package time_table;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@SpringBootApplication
public class TimeTableGeneratorSchApplication {

    public static void main(String[] args) {
        SpringApplication.run(TimeTableGeneratorSchApplication.class, args);
        log.info("\n\n*** TIME-TABLE-GENERATOR run! ***\n\n");
    }

}
