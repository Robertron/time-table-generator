package time_table.exception;

public class ExcelFileNotFoundException extends RuntimeException {

    public ExcelFileNotFoundException(String message) {
        super(message);
    }
}
