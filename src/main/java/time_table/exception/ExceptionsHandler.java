package time_table.exception;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.ConstraintViolationException;
import jakarta.validation.ValidationException;
import lombok.*;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@RestControllerAdvice
public class ExceptionsHandler extends ResponseEntityExceptionHandler {

    @Builder
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @FieldDefaults(
            level = AccessLevel.PRIVATE
    )
    public static class ExceptionDetail {

        @JsonProperty(value = "status")
        HttpStatus status;

        @JsonProperty(value = "timestamp")
        Date timestamp;

        @JsonProperty(value = "comment")
        String comment;

        @JsonProperty(value = "message")
        String message;

        @JsonProperty(value = "details")
        String details;

        {
            this.timestamp = new Date();
        }
    }

    //Always 400
    protected ResponseEntity<Object> handleExceptionInternal(Exception ex,
                                                             @Nullable Object body,
                                                             HttpHeaders headers,
                                                             HttpStatus status,
                                                             WebRequest request) {

        return new ResponseEntity<>(body, headers, status);
    }

    protected ResponseEntity<Object> handleBindException(BindException ex,
                                                         HttpHeaders headers,
                                                         HttpStatus status,
                                                         WebRequest request) {
        ExceptionDetail exceptionDetail = ExceptionDetail.builder()
                .status(HttpStatus.BAD_REQUEST)
                .comment("The API execution error - wrong request param")
                .message(String.valueOf(ex.getAllErrors().stream()
                        .map(e -> new StringBuilder(Objects.requireNonNull(e.getDefaultMessage())))
                        .reduce(new StringBuilder(), (builder, error) -> {
                            if (builder.length() > 0) {
                                builder.append(", ");
                            }
                            builder.append(error);
                            return builder;
                        })))
                .details(request.getDescription(true))
                .timestamp(new Date())
                .build();
        return handleExceptionInternal(ex, exceptionDetail, headers, exceptionDetail.getStatus(), request);
    }

    /*
     * Handling errors in API requests
     * BindException - thrown in case of fatal binding errors
     * MethodArgumentNotValidException - thrown when an argument annotated with @Valid fails validation
     */
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException e,
                                                                  HttpHeaders headers,
                                                                  HttpStatus status,
                                                                  WebRequest request) {
        List<String> messages = new ArrayList<>();

        int numberOfErrorMessage = 1;
        for (FieldError error : e.getBindingResult().getFieldErrors()) {
            if (e.getBindingResult().getFieldErrors().size() > 1) {
                messages.add(String.format(
                        "Wrong request param #%s - %s",
                        numberOfErrorMessage,
                        error.getDefaultMessage())
                );
            } else {
                messages.add(error.getDefaultMessage());
            }
            numberOfErrorMessage++;
        }

        numberOfErrorMessage = 1;
        for (ObjectError error : e.getBindingResult().getGlobalErrors()) {
            messages.add(String.format(
                    "Wrong request #%s - %s",
                    numberOfErrorMessage,
                    error.getDefaultMessage())
            );
            numberOfErrorMessage++;
        }

        String stackTrace = Arrays.stream(e.getStackTrace())
                .map(stackTraceElement -> stackTraceElement.toString() + " \n ")
                .collect(Collectors.joining());

        ExceptionDetail exceptionDetail = ExceptionDetail.builder()
                .status(HttpStatus.BAD_REQUEST)
                .comment("The API execution error - wrong request param")
                .message(String.join("<br />", messages))
                .details(request.getDescription(true))
                .timestamp(new Date())
                .build();

        return handleExceptionInternal(e, exceptionDetail, headers, exceptionDetail.getStatus(), request);
    }

    /*
     * Handling errors in API requests
     * MissingServletRequestPartException - thrown when a part of a composite request is missing (parameter is absent)
     */
    protected ResponseEntity<Object> handleMissingServletRequestParameter(MissingServletRequestParameterException e,
                                                                          HttpHeaders headers,
                                                                          HttpStatus status,
                                                                          WebRequest request) {

        String message = String.format("The param is lost - %s", e.getParameterName());

        String stackTrace = Arrays.stream(e.getStackTrace())
                .map(stackTraceElement -> stackTraceElement.toString() + " \n ")
                .collect(Collectors.joining());

        ExceptionDetail exceptionDetail = ExceptionDetail.builder()
                .status(HttpStatus.BAD_REQUEST)
                .comment("The API execution error - the param is lost")
                .message(message)
                .details(request.getDescription(true))
                .timestamp(new Date())
                .build();

        return handleExceptionInternal(e, exceptionDetail, headers, exceptionDetail.getStatus(), request);
    }

    /**
     * Handle constraint violation response entity.
     *
     * @param e       the e
     * @param request the request
     * @return the response entity
     */
    /*
     * Handling errors in API requests
     * ConstraintViolationException - reports the result of a violation of validation constraints on request values
     */
    @ExceptionHandler({
            ConstraintViolationException.class
    })
    public ResponseEntity<Object> handleConstraintViolation(ConstraintViolationException e, WebRequest request) {
        List<String> messages = new ArrayList<>();

        int numberOfErrorMessage = 1;
        for (ConstraintViolation<?> violation : e.getConstraintViolations()) {
            messages.add(String.format(
                    "Wrong request paramа #%s - %s",
                    numberOfErrorMessage,
                    violation.getMessage())
            );
            numberOfErrorMessage++;
        }

        String stackTrace = Arrays.stream(e.getStackTrace())
                .map(stackTraceElement -> stackTraceElement.toString() + " \n ")
                .collect(Collectors.joining());

        ExceptionDetail exceptionDetail = ExceptionDetail.builder()
                .status(HttpStatus.BAD_REQUEST)
                .comment("The API execution error - wrong request param")
                .message(String.join("<br />", messages))
                .details(request.getDescription(true))
                .timestamp(new Date())
                .build();

        return handleExceptionInternal(e, exceptionDetail, new HttpHeaders(), exceptionDetail.getStatus(), request);
    }

    /**
     * Handle method argument type mismatch response entity.
     *
     * @param e       the e
     * @param request the request
     * @return the response entity
     */
    /*
     * Handling errors in API requests
     * TypeMismatchException - thrown when attempting to set a bean property with an incorrect type.
     * MethodArgumentTypeMismatchException - thrown when a method argument is not of the expected type
     */
    @ExceptionHandler({
            MethodArgumentTypeMismatchException.class
    })
    public ResponseEntity<Object> handleMethodArgumentTypeMismatch(MethodArgumentTypeMismatchException e,
                                                                   WebRequest request) {

        String type = "array";
        if (e.getRequiredType() == String.class)
            type = "string";
        else if (e.getRequiredType() == Date.class
                || e.getRequiredType() == Calendar.class
                || e.getRequiredType() == Timestamp.class)
            type = "date";
        else if (e.getRequiredType() == Integer.class
                || e.getRequiredType() == Long.class
                || e.getRequiredType() == Short.class
                || e.getRequiredType() == Byte.class
                || e.getRequiredType() == Double.class
                || e.getRequiredType() == BigDecimal.class)
            type = "digit";

        String message = String.format("The param %s must to be define as %s", e.getName(), type);

        String stackTrace = Arrays.stream(e.getStackTrace())
                .map(stackTraceElement -> stackTraceElement.toString() + " \n ")
                .collect(Collectors.joining());

        ExceptionDetail exceptionDetail = ExceptionDetail.builder()
                .status(HttpStatus.BAD_REQUEST)
                .comment("The API execution error - wrong request param format")
                .message(message)
                .details(request.getDescription(true))
                .timestamp(new Date())
                .build();

        return handleExceptionInternal(e, exceptionDetail, new HttpHeaders(), exceptionDetail.getStatus(), request);
    }

    /**
     * Handle sql exception response entity.
     *
     * @param e       the e
     * @param request the request
     * @return the response entity
     */
    /*
     * Handling SQL errors
     */
    @ExceptionHandler({
            SQLException.class
    })
    public ResponseEntity<Object> handleSqlException(MethodArgumentTypeMismatchException e,
                                                     WebRequest request) {

        String message = "The wrong request params - please check data";

        String stackTrace = Arrays.stream(e.getStackTrace())
                .map(stackTraceElement -> stackTraceElement.toString() + " \n ")
                .collect(Collectors.joining());

        ExceptionDetail exceptionDetail = ExceptionDetail.builder()
                .status(HttpStatus.BAD_REQUEST)
                .comment("The API execution error - the sql execution error")
                .message(message)
                .details(request.getDescription(true))
                .timestamp(new Date())
                .build();

        return handleExceptionInternal(e, exceptionDetail, new HttpHeaders(), exceptionDetail.getStatus(), request);
    }

    /**
     * Handle illegal arguments response entity.
     *
     * @param e       the e
     * @param request the request
     * @return the response entity
     */
    @ExceptionHandler({
            IllegalArgumentException.class,
            ValidationException.class
    })
    public final ResponseEntity<Object> handleIllegalArguments(Exception e, WebRequest request) {
        log.error("exception - " + request, e);
        String[] messageParts = e.getMessage().split(": ");

        String message = "";
        for (int i = 1; i < messageParts.length; i++) {
            message += messageParts[i];
        }

        String stackTrace = Arrays.stream(e.getStackTrace())
                .map(stackTraceElement -> stackTraceElement.toString() + " \n ")
                .collect(Collectors.joining());

        ExceptionDetail exceptionDetail = ExceptionDetail.builder()
                .status(HttpStatus.BAD_REQUEST)
                .comment("The API execution error - illegal arguments")
                .message(message)
                .details(request.getDescription(true))
                .timestamp(new Date())
                .build();

        return handleExceptionInternal(e, exceptionDetail, new HttpHeaders(), exceptionDetail.getStatus(), request);
    }

    @ExceptionHandler(ConflictException.class)
    public final ResponseEntity<Object> handleConflictException(ConflictException e, WebRequest request) {
        ExceptionDetail exceptionDetail = ExceptionDetail.builder()
                .status(HttpStatus.CONFLICT)
                .message(e.getMessage())
                .details(request.getDescription(true))
                .timestamp(new Date())
                .build();

        return handleExceptionInternal(e, exceptionDetail, new HttpHeaders(), exceptionDetail.getStatus(), request);
    }

    @ExceptionHandler(BadRequestException.class)
    public final ResponseEntity<Object> handleBadRequestException(BadRequestException e, WebRequest request) {
        ExceptionDetail exceptionDetail = ExceptionDetail.builder()
                .status(HttpStatus.BAD_REQUEST)
                .message(e.getMessage())
                .details(request.getDescription(true))
                .timestamp(new Date())
                .build();

        return handleExceptionInternal(e, exceptionDetail, new HttpHeaders(), exceptionDetail.getStatus(), request);
    }

    @ExceptionHandler(NoSuchElementException.class)
    public final ResponseEntity<Object> handleNoSuchElementException(NoSuchElementException e, WebRequest request) {
        ExceptionDetail exceptionDetail = ExceptionDetail.builder()
                .status(HttpStatus.NOT_FOUND)
                .message(e.getMessage())
                .details(request.getDescription(true))
                .timestamp(new Date())
                .build();

        return handleExceptionInternal(e, exceptionDetail, new HttpHeaders(), exceptionDetail.getStatus(), request);
    }

    @ExceptionHandler(HttpClientErrorException.Forbidden.class)
    public final ResponseEntity<Object> handleForbiddenException(HttpClientErrorException.Forbidden e, WebRequest request) {
        String stackTrace = Arrays.stream(e.getStackTrace())
                .map(stackTraceElement -> stackTraceElement.toString() + " \n ")
                .collect(Collectors.joining());

        ExceptionDetail exceptionDetail = ExceptionDetail.builder()
                .status(HttpStatus.FORBIDDEN)
                .message(e.getMessage())
                .details(request.getDescription(true))
                .timestamp(new Date())
                .build();

        return handleExceptionInternal(e, exceptionDetail, new HttpHeaders(), exceptionDetail.getStatus(), request);
    }

    @ExceptionHandler({
            ExcelFileExpectedException.class,
            ExcelFileNotFoundException.class,
            ExcelFileValidationException.class
    })
    public final ResponseEntity<Object> handleFileException(RuntimeException e, WebRequest request) {
        ExceptionDetail exceptionDetail = ExceptionDetail.builder()
                .status(HttpStatus.NOT_FOUND)
                .message(e.getMessage())
                .details(request.getDescription(true))
                .timestamp(new Date())
                .build();

        return handleExceptionInternal(e, exceptionDetail, new HttpHeaders(), exceptionDetail.getStatus(), request);
    }
}