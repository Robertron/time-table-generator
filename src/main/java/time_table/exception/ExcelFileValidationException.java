package time_table.exception;

public class ExcelFileValidationException extends RuntimeException {

    public ExcelFileValidationException(String message) {
        super(message);
    }
}
