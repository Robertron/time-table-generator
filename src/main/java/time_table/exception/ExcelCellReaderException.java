package time_table.exception;

import lombok.Getter;
import org.dhatim.fastexcel.reader.ExcelReaderException;

@Getter
public class ExcelCellReaderException extends ExcelReaderException {

    private final int cellNum;

    public ExcelCellReaderException(String m, int cellNum) {
        super(m);
        this.cellNum = cellNum;
    }
}
