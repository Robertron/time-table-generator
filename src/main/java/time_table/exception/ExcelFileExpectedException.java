package time_table.exception;

public class ExcelFileExpectedException extends RuntimeException {

    public ExcelFileExpectedException() {
        super("The file must be of type xlsx");
    }
}
