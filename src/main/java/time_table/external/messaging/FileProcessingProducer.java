package time_table.external.messaging;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;
import time_table.domain.dto.TimeTableDto;
import time_table.exception.ConflictException;
import time_table.external.messaging.dto.ExcelFileProcessingMessage;

import java.util.List;

@Slf4j

@Service
public class FileProcessingProducer {

    @Value("${broker.queue.excelProcessingQueue}")
    private String queue;
    private final ObjectMapper objectMapper;
    private final RabbitTemplate rabbitTemplate;

    public FileProcessingProducer(
            ObjectMapper objectMapper,
            RabbitTemplate rabbitTemplate
    ) {
        this.objectMapper = objectMapper;
        this.rabbitTemplate = rabbitTemplate;
    }

    public  List<TimeTableDto> sendAndReceive(ExcelFileProcessingMessage message) {
        ParameterizedTypeReference<List<TimeTableDto>> typeReference = new ParameterizedTypeReference<>() {};
        String messageAsString;
        try {
            messageAsString = objectMapper.writeValueAsString(message);
        } catch (JsonProcessingException e) {
            log.error(e.getMessage());
            throw new ConflictException(e.getMessage());
        }
        List<TimeTableDto> response = rabbitTemplate.convertSendAndReceiveAsType(queue, messageAsString, typeReference);
        log.info(">> Message to rabbit was sent and the response was returned: {}", response);
        return response;
    }

}
