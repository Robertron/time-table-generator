package time_table.external.messaging;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;
import time_table.converter.ExcelDocumentProcessor;
import time_table.converter.row.CharacteristicsRow;
import time_table.converter.row.LecturesRow;
import time_table.converter.row.SubGroupsRow;
import time_table.domain.data_arrangements.GroupTable;
import time_table.domain.data_arrangements.GroupWithSubGroupsTable;
import time_table.domain.dto.SubGroupTableDto;
import time_table.domain.dto.TimeTableDto;
import time_table.external.messaging.dto.ExcelFileProcessingMessage;
import time_table.util.ScheduleGeneratorHelper;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static time_table.Generator.ScheduleGenerator.generateScheduleForGroups;
import static time_table.Generator.ScheduleGenerator.generateScheduleForSubGroups;
import static time_table.util.FileHelper.getFile;
import static time_table.util.ScheduleGeneratorHelper.*;

@Slf4j
@RequiredArgsConstructor
@Service
public class FileProcessingConsumer {

    private final ObjectMapper objectMapper;
    private final ExcelDocumentProcessor processor;

    @RabbitListener(queues = {
            "#{'${broker.queue.excelProcessingQueue}'}"
    })
    public List<TimeTableDto> listen(String messageAsString) {
        ArrayList<List<?>> tabsList;
        ExcelFileProcessingMessage message = getMessage(messageAsString);
        File file = getFile(message.pathToFile());
        String template = message.template();
        try {
            tabsList = processor.processDoc(template, file);
            log.info("<< Import result - {}", true);
            log.info("<< The TabsList is the following: {} ", tabsList.toString());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        // Find the list of subGroup objects
        List<SubGroupsRow> subGroups = getRowList(tabsList, SubGroupsRow.class);
        log.info("SUBGROUPS {}", subGroups);

        // Find the list of lecture objects
        List<LecturesRow> lectures = getRowList(tabsList, LecturesRow.class);
        log.info("LECTURES {}", lectures);

        // Find the list of Characteristics objects
        List<CharacteristicsRow> characteristics = getRowList(tabsList, CharacteristicsRow.class);
        log.info("CHARACTERISTICS {}", characteristics);

        int periodsForLecturesAndTutorials = ScheduleGeneratorHelper.calculatePeriodsForLecturesAndTutorials(
                lectures,
                characteristics
        );

        List<SubGroupTableDto> subGroupTimeTable = generateScheduleForSubGroups(
                subGroups,
                lectures,
                characteristics,
                periodsForLecturesAndTutorials
        );

        List<GroupTable> groupTimeTable = generateScheduleForGroups(
                lectures,
                characteristics,
                periodsForLecturesAndTutorials
        );

        List<GroupWithSubGroupsTable> groupWithSubGroups = assignGroupToSubGroupTimeTable(tabsList, subGroupTimeTable);

        return joinTimeTableGenerations(groupTimeTable, groupWithSubGroups);
    }
    private ExcelFileProcessingMessage getMessage(String message) {
        ExcelFileProcessingMessage processingPayload;
        try {
            processingPayload = objectMapper.readValue(message, ExcelFileProcessingMessage.class);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        return processingPayload;
    }
}
