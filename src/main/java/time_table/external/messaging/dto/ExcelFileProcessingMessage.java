package time_table.external.messaging.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

public record ExcelFileProcessingMessage(String pathToFile, Long fileSize, String template) {
    public ExcelFileProcessingMessage(
            @JsonProperty("pathToFile") String pathToFile,
            @JsonProperty("fileSize") Long fileSize,
            @JsonProperty("template") String template
    ) {
        this.pathToFile = pathToFile;
        this.fileSize = fileSize;
        this.template = template;
    }
}
