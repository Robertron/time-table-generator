package time_table.configuration;

import io.swagger.v3.oas.models.ExternalDocumentation;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SwaggerConfig {

    @Bean
    public OpenAPI springShopOpenAPI() {//https://stackoverflow.com/a/59995027, https://swagger.io/specification/
        return new OpenAPI()
                .info(new Info().title("TimeTable Algorithm API")
                        .description("Spring boot application to generate a time-table based on genetic algorithms")
                        .version("v0.0.1")
                        .termsOfService("/schedule/terms-of-service")
                        .contact(new Contact()
                                .name("Roberto Chavez")
                                .url("https://www.linkedin.com/in/roberto-enrique-chavez-rodriguez-4a017875/")
                                .email("r.chavez@innopolis.ru"))
                        .license(new License()
                                .name("Apache 2.0")
                                .url("http://springdoc.org")))
                .externalDocs(new ExternalDocumentation()
                        .description("SpringShop Wiki Documentation")
                        .url("https://springshop.wiki.github.org/docs"));
    }
}