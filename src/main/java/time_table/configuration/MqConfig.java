package time_table.configuration;

import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MqConfig {

    @Value("${broker.queue.excelProcessingQueue}")
    private String excelProcessingQueue;

    @Bean
    public Queue excelProcessingQueue() {
        return new Queue(excelProcessingQueue, false);
    }

}
