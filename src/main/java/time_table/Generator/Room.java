package time_table.Generator;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Objects;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Room {

    private Integer room;

    private Integer capacity;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Room room1)) return false;
        return Objects.equals(room, room1.room) &&
                Objects.equals(capacity, room1.capacity);
    }
    @Override
    public int hashCode() {
        return Objects.hash(room, capacity);
    }

}
