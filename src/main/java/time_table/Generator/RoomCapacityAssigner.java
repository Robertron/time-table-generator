package time_table.Generator;

import lombok.extern.slf4j.Slf4j;
import time_table.domain.data_arrangements.GroupTable;
import time_table.domain.dto.SubGroupTableDto;

import java.util.ArrayList;
import java.util.List;

@Slf4j
public class RoomCapacityAssigner {

    /**
     * The algorithm implements two conditions:
     * No room should be assigned at the same time (period) to two or more groups,
     * And each assigned room should have a capacity equal or superior to the number of students in one subject
     */
    public static List<SubGroupTableDto> assignRoomToSubGroups(List<SubGroupTableDto> filledTimeTable, List<Room> roomsInformation){

        int numberOfPeriodsInOneDay = filledTimeTable.get(0).getSubGroupStudyDtoList().get(0).getSubGroupPeriodDtos().size();
        int numberOfDays = filledTimeTable.get(0).getSubGroupStudyDtoList().size();

        for (int j = 0; j < numberOfDays; j++){
            int k = 0;
            while (k < numberOfPeriodsInOneDay) {
                List<Room> occupiedRooms = new ArrayList<>();
                List<Room> availableRooms = new ArrayList<>(roomsInformation);
                for (SubGroupTableDto tableDto: filledTimeTable){
                    Integer studentNumber = tableDto.getSubGroupStudyDtoList().get(j).getSubGroupPeriodDtos().get(k).getNumberOfStudents();
                    assignRoomToSubGroup(tableDto, studentNumber, availableRooms, occupiedRooms, k, j);
                }
                k++;
            }
        }
        return filledTimeTable;
    }

    public static List<GroupTable> assignRoomToGroups(List<GroupTable> filledGroupsTable, List<Room> roomsInformation){

        int numberOfPeriodsInOneDay = filledGroupsTable.get(0).getGroupStudyDtoList().get(0).getGroupPeriodDtos().size();
        int numberOfDays = filledGroupsTable.get(0).getGroupStudyDtoList().size();

        for (int j = 0; j < numberOfDays; j++){
            int k = 0;
            while (k < numberOfPeriodsInOneDay) {
                List<Room> occupiedRooms = new ArrayList<>();
                List<Room> availableRooms = new ArrayList<>(roomsInformation);
                for (GroupTable tableDto: filledGroupsTable){
                    Integer studentNumber = tableDto.getGroupStudyDtoList().get(j).getGroupPeriodDtos().get(k).getNumberOfStudents();
                    assignRoomToGroup(tableDto, studentNumber, availableRooms, occupiedRooms, k, j);
                }
                k++;
            }
        }
        return filledGroupsTable;
    }
    private static void assignRoomToSubGroup(
            SubGroupTableDto tableDto,
            Integer studentNumber,
            List<Room> availableRooms,
            List<Room> occupiedRooms,
            int k,
            int j
    ){
        if (studentNumber != null){
            Room candidateRoom = findMinimumRoom(availableRooms, studentNumber);
            if (candidateRoom == null){
               log.error("No suitable room has been found for the subject {} ",
                       tableDto.getSubGroupStudyDtoList().get(j).getSubGroupPeriodDtos().get(k).getSubjectLab()
               );
            } else {
                if (!occupiedRooms.contains(candidateRoom)){
                    tableDto.getSubGroupStudyDtoList().get(j).getSubGroupPeriodDtos().get(k).setRoom(candidateRoom.getRoom());
                    occupiedRooms.add(candidateRoom);
                } else {
                    availableRooms.remove(candidateRoom);
                    assignRoomToSubGroup(tableDto, studentNumber, availableRooms, occupiedRooms, k, j);
                }
            }
        }
    }

    private static void assignRoomToGroup(
            GroupTable groupTable,
            Integer studentNumber,
            List<Room> availableRooms,
            List<Room> occupiedRooms,
            int k,
            int j
    ){
        if (studentNumber != null){
            Room candidateRoom = findMinimumRoom(availableRooms, studentNumber);
            if (candidateRoom == null){
                log.error("No suitable room has been found for the subject {} ",
                        groupTable.getGroupStudyDtoList().get(j).getGroupPeriodDtos().get(k).getSubject()
                );
            } else {
                if (!occupiedRooms.contains(candidateRoom)){
                    groupTable.getGroupStudyDtoList().get(j).getGroupPeriodDtos().get(k).setRoom(candidateRoom.getRoom());
                    occupiedRooms.add(candidateRoom);
                } else {
                    availableRooms.remove(candidateRoom);
                    assignRoomToGroup(groupTable, studentNumber, availableRooms, occupiedRooms, k, j);
                }
            }
        }
    }

    private static Room findMinimumRoom(List<Room> roomList, int numberOfStudents) {
        Room minRoom = null;
        int minCapacity = Integer.MAX_VALUE;
        for (Room room : roomList) {
            if (room.getCapacity() >= numberOfStudents && room.getCapacity() < minCapacity) {
                minRoom = room;
                minCapacity = room.getCapacity();
            }
        }
        return minRoom;
    }
}
