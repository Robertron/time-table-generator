package time_table.Generator;

import lombok.extern.slf4j.Slf4j;
import time_table.Generator.core.Chromosome;
import time_table.Generator.core.Inputdata;
import time_table.Generator.core.SchedulerMain;
import time_table.converter.GenericGroupInformation;
import time_table.converter.row.CharacteristicsRow;
import time_table.converter.row.LecturesRow;
import time_table.converter.row.SubGroupsRow;
import time_table.domain.data_arrangements.GroupTable;
import time_table.domain.dto.SubGroupTableDto;

import java.util.*;

import static time_table.util.ScheduleGeneratorHelper.*;

@Slf4j
public class ScheduleGenerator {
    public static List<SubGroupTableDto> generateScheduleForSubGroups(
            List<SubGroupsRow> subGroups,
            List<LecturesRow> lectures,
            List<CharacteristicsRow> characteristics,
            int periodsForLecturesAndTutorials
    ) {

        //Generate the list of GenericGroupInformation objects
        List<GenericGroupInformation> subGroupsInformation = mapToSubGroupsInformation(subGroups, lectures);

        //invokes method to take input for subgroups
        new Inputdata().takeInput(
                characteristics,
                subGroupsInformation,
                periodsForLecturesAndTutorials,
                false
        );

        //invokes algorithm
        new SchedulerMain();

        //grabs final chromosome i.e. the output
        Chromosome finalSon =SchedulerMain.finalson;

        log.info("FinalSon is: {}", finalSon);

        return RoomCapacityAssigner.assignRoomToSubGroups(
                finalSon.fillSubGroupsTable(subGroupsInformation, characteristics, periodsForLecturesAndTutorials),
                getRoomInformation(characteristics)
        );
    }

    public static List<GroupTable> generateScheduleForGroups(
            List<LecturesRow> lectures,
            List<CharacteristicsRow> characteristics,
            int periodsForLecturesAndTutorials
    ) {
        //Takes lectures and tutorials that have at least one lesson
        List<LecturesRow> lecturesForGroupSchedule = lectures.stream()
                .filter(lecture -> lecture.getLecturesInAWeek() + lecture.getTutorialsInAWeek() > 0)
                .toList();

        //Generate the list of GenericGroupInformation objects
        List<GenericGroupInformation> groupsInformation = mapToGroupsInformation(lecturesForGroupSchedule);

        //invokes method to take input for groups
        new Inputdata().takeInput(
                characteristics,
                groupsInformation,
                periodsForLecturesAndTutorials,
                true
        );
        //invokes algorithm
        new SchedulerMain();

        //grabs final chromosome i.e. the output
        Chromosome finalSon = SchedulerMain.finalson;

        return RoomCapacityAssigner.assignRoomToGroups(
                finalSon.fillGroupsTable(groupsInformation, characteristics),
                getRoomInformation(characteristics)
        );
    }
}
