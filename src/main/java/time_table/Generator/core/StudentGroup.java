package time_table.Generator.core;

public class StudentGroup {
	public int id;
	public String name;
	public String[] subjectsInGroup;
	public int numberOfSubjects;
	public int[] teacherid;
	public int[] subjectPeriodsInWeek;
	
	public StudentGroup() {
		subjectsInGroup =new String[200];
		subjectPeriodsInWeek =new int[200];
		teacherid=new int[200];
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String[] getSubjectsInGroup() {
		return subjectsInGroup;
	}
	public void setSubjectsInGroup(String[] subjectsInGroup) {
		this.subjectsInGroup = subjectsInGroup;
	}
	public int getNumberOfSubjects() {
		return numberOfSubjects;
	}
	public void setNumberOfSubjects(String snosubject) {
		this.numberOfSubjects = Integer.parseInt(snosubject);
	}
	public int[] getTeacherid() {
		return teacherid;
	}
	public void setTeacherid(int[] teacherid) {
		this.teacherid = teacherid;
	}
	public int[] getSubjectPeriodsInWeek() {
		return subjectPeriodsInWeek;
	}
	public void setSubjectPeriodsInWeek(int[] subjectPeriodsInWeek) {
		this.subjectPeriodsInWeek = subjectPeriodsInWeek;
	}
	
}
