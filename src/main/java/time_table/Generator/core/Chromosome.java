package time_table.Generator.core;

import time_table.converter.GenericGroupInformation;
import time_table.converter.row.CharacteristicsRow;
import time_table.domain.data_arrangements.GroupTable;
import time_table.domain.dto.*;

import java.io.*;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static time_table.domain.enums.DayOfTheWeek.*;

//Chromosome represents array of genes as complete timetable (looks like gene[0]gene[1]gene[2]...)
public class Chromosome implements Comparable<Chromosome>,Serializable{
	
	static double crossoverrate= Inputdata.crossoverrate;
	static double mutationrate= Inputdata.mutationrate;
	static int numberOfPeriodsPerDay = Inputdata.timeSlotsPerDay, days= Inputdata.daysPerWeek;
	static int nostgrp= Inputdata.numberOfStudentGroups;
	double fitness;
	int point;
	
	public Gene[] gene;
	
	Chromosome(){
		nostgrp = Inputdata.numberOfStudentGroups;
		
		gene=new Gene[nostgrp];
		
		for(int i=0;i<nostgrp;i++){
			
			gene[i]=new Gene(i);
			
			//System.out.println("");
		}
		fitness=getFitness();		
		
	}
	
	public Chromosome deepClone() {
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(baos);
			oos.writeObject(this);

			ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
			ObjectInputStream ois = new ObjectInputStream(bais);
			return (Chromosome) ois.readObject();
		} catch (IOException | ClassNotFoundException e) {
			return null;
		}
	}

	public double getFitness(){
		numberOfPeriodsPerDay = Inputdata.timeSlotsPerDay;
		days= Inputdata.daysPerWeek;
		point=0;
		for(int i = 0; i< numberOfPeriodsPerDay *days; i++){
			
			List<Integer> teacherlist=new ArrayList<Integer>();
			
			for(int j=0;j<nostgrp;j++){
			
				Slot slot;
				//System.out.println("i="+i+" j="+j);
				if(TimeTable.slot[gene[j].slotno[i]]!=null)
					slot=TimeTable.slot[gene[j].slotno[i]];
				else slot=null;

				if(slot!=null){
				
					if(teacherlist.contains(slot.teacherid)){
						point++;
					}
					else teacherlist.add(slot.teacherid);
				}
			}	
			
			
		}
		//System.out.println(point);
		if (nostgrp > 1) {
			fitness=1-(point/((nostgrp-1.0)* numberOfPeriodsPerDay *days));
		} else {
			fitness = 1; // Set fitness to 1 when nostgrp is 1 (avoid division by zero)
		}
		point=0;
		return fitness;
	}
	
	
	
	//printing final timetable
	public void printTimeTable(){
		
		//for each student group separate time table
		for(int i=0;i<nostgrp;i++){
			
			//status used to get name of student grp because in case first class is free it will throw error
			boolean status=false;
			int l=0;
			while(!status){
				
				//printing name of batch
				if(TimeTable.slot[gene[i].slotno[l]]!=null){
					System.out.println("Batch "+TimeTable.slot[gene[i].slotno[l]].studentgroup.name+" Timetable-");
					
					status=true;
				}
				l++;
			
			}
			
			//looping for each day
			for(int j=0;j<days;j++){
				
				//looping for each hour of the day
				for(int k = 0; k< numberOfPeriodsPerDay; k++){
				
					//checking if this slot is free otherwise printing it
					if(TimeTable.slot[gene[i].slotno[k+j* numberOfPeriodsPerDay]]!=null)
						
						System.out.print(TimeTable.slot[gene[i].slotno[k+j* numberOfPeriodsPerDay]].subject+" ");
				
					else System.out.print("*FREE* ");
				
				}
				
				System.out.println();
			}
			
			System.out.println("\n\n\n");
		
		}

	}
	public List<SubGroupTableDto> fillSubGroupsTable(
			List<GenericGroupInformation> labs,
			List<CharacteristicsRow> characteristics,
			int periodsForLecturesAndTutorials
	) {
		List<SubGroupTableDto> subGroupTableDtoList = new ArrayList<>();
		//for each student group separate timetable
		for(int i=0;i<nostgrp;i++){
			SubGroupTableDto subGroupTableDto = new SubGroupTableDto();

			//status used to get name of student grp because in case first class is free it will throw error
			boolean status=false;
			int l=0;
			while(!status){

				//printing name of batch
				if(TimeTable.slot[gene[i].slotno[l]]!=null){
					subGroupTableDto.setSubGroup(TimeTable.slot[gene[i].slotno[l]].studentgroup.name);

					status=true;
				}
				l++;

			}
			List<SubGroupStudyDto> subGroupStudyDtos = new ArrayList<>();

			//looping for each day
			for(int j=0;j<days;j++){
				SubGroupStudyDto subGroupStudyDto = new SubGroupStudyDto();
				List<SubGroupPeriodDto> subGroupPeriodDtos = new ArrayList<>();

				subGroupStudyDto.setDay(setDayOfTheWeek(j+1));

				//Looping for each period of the day
				int labPeriodsStart = periodsForLecturesAndTutorials;
				for(int k = 0; k< numberOfPeriodsPerDay; k++){
					SubGroupPeriodDto subGroupPeriodDto = new SubGroupPeriodDto();
					subGroupPeriodDto.setPeriod(String.format("Period %d", k+1));
					subGroupPeriodDto.setStartTime(setPeriodStartTime(labPeriodsStart, characteristics));
					subGroupPeriodDto.setEndTime(setPeriodEndTime(labPeriodsStart, characteristics));

					//checking if this slot is free otherwise printing it
					if(TimeTable.slot[gene[i].slotno[k+j* numberOfPeriodsPerDay]]!=null){
						subGroupPeriodDto.setSubjectLab(TimeTable.slot[gene[i].slotno[k+j* numberOfPeriodsPerDay]].subject);
						subGroupPeriodDto.setNumberOfStudents(
								setNumberOfStudentsPerSubGroupAndSubject(
										labs,
										subGroupTableDto.getSubGroup(),
										subGroupPeriodDto.getSubjectLab()
								)
						);
						subGroupPeriodDto.setAssistantName(Inputdata.teacher[TimeTable.slot[gene[i].slotno[k+j* numberOfPeriodsPerDay]].teacherid].getName());

					} else {
						subGroupPeriodDto.setSubjectLab("*FREE*");
					}
					subGroupPeriodDtos.add(subGroupPeriodDto);

					labPeriodsStart++;
				}
				subGroupStudyDto.setSubGroupPeriodDtos(subGroupPeriodDtos);
				subGroupStudyDtos.add(subGroupStudyDto);
			}
			subGroupTableDto.setSubGroupStudyDtoList(subGroupStudyDtos);

			subGroupTableDtoList.add(subGroupTableDto);
		}
		return subGroupTableDtoList;
	}

	public List<GroupTable> fillGroupsTable(
			List<GenericGroupInformation> genericGroups,
			List<CharacteristicsRow> characteristics
	) {
		List<GroupTable> groupTableList = new ArrayList<>();
		//for each student group separate timetable
		for(int i=0;i<nostgrp;i++){
			GroupTable groupTable = new GroupTable();

			//status used to get name of student grp because in case first class is free it will throw error
			boolean status=false;
			int l=0;
			while(!status){

				//printing name of batch
				if(TimeTable.slot[gene[i].slotno[l]]!=null){
					groupTable.setGroup(TimeTable.slot[gene[i].slotno[l]].studentgroup.name);

					status=true;
				}
				l++;

			}
			List<GroupStudyDto> subGroupStudyDtos = new ArrayList<>();

			//looping for each day
			for(int j=0;j<days;j++){
				GroupStudyDto groupStudyDto = new GroupStudyDto();
				List<GroupPeriodDto> groupPeriodDtos = new ArrayList<>();

				groupStudyDto.setDay(setDayOfTheWeek(j+1));

				//Looping for each period of the day
				for(int k = 0; k< numberOfPeriodsPerDay; k++){
					GroupPeriodDto groupPeriodDto = new GroupPeriodDto();
					groupPeriodDto.setPeriod(String.format("Period %d", k+1));
					groupPeriodDto.setStartTime(setPeriodStartTime(k, characteristics));
					groupPeriodDto.setEndTime(setPeriodEndTime(k, characteristics));

					//checking if this slot is free otherwise printing it
					if(TimeTable.slot[gene[i].slotno[k+j* numberOfPeriodsPerDay]]!=null){
						groupPeriodDto.setSubject(TimeTable.slot[gene[i].slotno[k+j* numberOfPeriodsPerDay]].subject);
						groupPeriodDto.setNumberOfStudents(
								setNumberOfStudentsPerSubGroupAndSubject(
										genericGroups,
										groupTable.getGroup(),
										groupPeriodDto.getSubject()
								)
						);
						groupPeriodDto.setInstructorName(Inputdata.teacher[TimeTable.slot[gene[i].slotno[k+j* numberOfPeriodsPerDay]].teacherid].getName());

					} else {
						groupPeriodDto.setSubject("*FREE*");
					}
					groupPeriodDtos.add(groupPeriodDto);
				}
				groupStudyDto.setGroupPeriodDtos(groupPeriodDtos);
				subGroupStudyDtos.add(groupStudyDto);
			}
			groupTable.setGroupStudyDtoList(subGroupStudyDtos);

			groupTableList.add(groupTable);
		}
		return groupTableList;
	}
	
	
	public void printChromosome(){
		
		for(int i=0;i<nostgrp;i++){
			for(int j = 0; j< numberOfPeriodsPerDay *days; j++){
				System.out.print(gene[i].slotno[j]+" ");
			}
			System.out.println("");
		}
		
	}

	public int compareTo(Chromosome c) {
		return Double.compare(c.fitness, fitness);
	}
	private Integer setNumberOfStudentsPerSubGroupAndSubject(List<GenericGroupInformation> subjects, String studentGroup, String subject) {
		Optional<Integer> numberOfStudentsInSubject = subjects.stream()
				.filter(a -> studentGroup.equals(a.getSubGroup()) && subject.equals(a.getSubjectName()))
				.map(GenericGroupInformation::getNumberOfStudents)
				.findFirst();
		return numberOfStudentsInSubject.orElseThrow(
				() -> new IllegalArgumentException(
						"The number of students for group " + studentGroup + "and subject " + subject + "was not given!"
				)
		);
	}
	private LocalTime setPeriodStartTime(int numberOfPeriod, List<CharacteristicsRow> characteristics){
		return characteristics.get(numberOfPeriod).getPeriodStartTime();
	}
	private LocalTime setPeriodEndTime(int numberOfPeriod, List<CharacteristicsRow> characteristics){
		return characteristics.get(numberOfPeriod).getPeriodEndTime();
	}
	private String setDayOfTheWeek(int dayNumber){
		return switch (dayNumber) {
			case 1 -> MONDAY.toString();
			case 2 -> TUESDAY.toString();
			case 3 -> WEDNESDAY.toString();
			case 4 -> THURSDAY.toString();
			case 5 -> FRIDAY.toString();
			case 6 -> SATURDAY.toString();
			default -> SUNDAY.toString();
		};
	}
	
}