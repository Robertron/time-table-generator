package time_table.Generator.core;

public class Utility {
	
	public static void printInputData(){
		System.out.println("Nostgrp="+ Inputdata.numberOfStudentGroups +" Noteachers="+ Inputdata.numberOfTeachers +" daysperweek="+ Inputdata.daysPerWeek +" hoursperday="+ Inputdata.timeSlotsPerDay);
		for(int i = 0; i< Inputdata.numberOfStudentGroups; i++){
			
			System.out.println(Inputdata.studentgroup[i].id+" "+ Inputdata.studentgroup[i].name);
			
			for(int j = 0; j< Inputdata.studentgroup[i].numberOfSubjects; j++){
				System.out.println(Inputdata.studentgroup[i].subjectsInGroup[j]+" "+ Inputdata.studentgroup[i].subjectPeriodsInWeek[j]+" hrs "+ Inputdata.studentgroup[i].teacherid[j]);
			}
			System.out.println("");
		}
		
		for(int i = 0; i< Inputdata.numberOfTeachers; i++){
			System.out.println(Inputdata.teacher[i].id+" "+ Inputdata.teacher[i].name+" "+ Inputdata.teacher[i].subject+" "+ Inputdata.teacher[i].assigned);
		}
	}
	
	
	public static void printSlots(){
		
		int days= Inputdata.daysPerWeek;
		int hours= Inputdata.timeSlotsPerDay;
		int nostgrp= Inputdata.numberOfStudentGroups;
		System.out.println("----Slots----");
		for(int i=0;i<days*hours*nostgrp;i++){
			if(TimeTable.slot[i]!=null)
				System.out.println(i+"- "+TimeTable.slot[i].studentgroup.name+" "+TimeTable.slot[i].subject+" "+TimeTable.slot[i].teacherid);
			else
				System.out.println("Free Period");
			if((i+1)%(hours*days)==0) System.out.println("******************************");
		}
		
	}
	
	
	
}
