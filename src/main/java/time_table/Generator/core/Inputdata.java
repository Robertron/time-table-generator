package time_table.Generator.core;

import org.apache.commons.lang3.ArrayUtils;
import time_table.converter.row.CharacteristicsRow;
import time_table.converter.GenericGroupInformation;

import java.util.*;
import java.util.stream.Collectors;

public class Inputdata {

	public static StudentGroup[] studentgroup;
	public static Teacher[] teacher;
	public static double crossoverrate = 1.0, mutationrate = 0.1;
	public static int numberOfStudentGroups, numberOfTeachers;
	public static int timeSlotsPerDay, daysPerWeek;

	public Inputdata() {
		studentgroup = new StudentGroup[200];
		teacher =   new Teacher[300];
	}

	boolean classformat(String l) {
		StringTokenizer st = new StringTokenizer(l, " ");
		return st.countTokens() == 3;
	}

	// Takes input
	public void takeInput(
			List<CharacteristicsRow> characteristics,
			List<GenericGroupInformation> genericGroupInformation,
			int periodsForLecturesAndTutorials,
			boolean isGroup
	) {
		//Input of Time slots per day and days per week
		if (isGroup){
			timeSlotsPerDay = periodsForLecturesAndTutorials;
		} else {
			timeSlotsPerDay = characteristics.get(0).getTimePeriodsPerDay() - periodsForLecturesAndTutorials;
		}

		daysPerWeek = characteristics.get(0).getDaysPerWeek();
		try {
			//Input of lab assistants
			int facultyCounter = 0;

			while (facultyCounter < genericGroupInformation.size()){
				teacher[facultyCounter] = new Teacher();
				teacher[facultyCounter].id = facultyCounter;
				teacher[facultyCounter].name = genericGroupInformation.get(facultyCounter).getInstructor();
				teacher[facultyCounter].subject = genericGroupInformation.get(facultyCounter).getSubjectName();
				facultyCounter++;
			}

			numberOfTeachers = facultyCounter;

			// input student group
			Set<String> studentGroups = genericGroupInformation.stream()
					.map(GenericGroupInformation::getSubGroup)
					.collect(Collectors.toSet());

			List<String> studentGroupsList = new ArrayList<>(studentGroups);

			for (int i = 0; i < studentGroups.size(); i++) {
				String studentGroupName = studentGroupsList.get(i);
				studentgroup[i] = new StudentGroup();
				studentgroup[i].id = i;
				studentgroup[i].name = studentGroupName;
				studentgroup[i].numberOfSubjects = countNumberOfSubjectsPerSubGroupOrGroup(genericGroupInformation, studentGroupName);
				studentgroup[i].subjectsInGroup = getSubjectsInSubGroupOrGroup(genericGroupInformation, studentGroupName);
				studentgroup[i].subjectPeriodsInWeek = getArrayHours(genericGroupInformation, studentgroup[i].subjectsInGroup, studentGroupName);
			}
			numberOfStudentGroups = studentGroups.size();

		} catch (Exception e) {
			e.printStackTrace();
		}
		assignTeacher();
	}

	// assigning a teacher for each subject for every studentgroup
	public void assignTeacher() {

		// looping through every studentgroup
		for (int i = 0; i < numberOfStudentGroups; i++) {

			// looping through every subject of a student group
			for (int j = 0; j < studentgroup[i].numberOfSubjects; j++) {

				int teacherid = -1;
				int assignedmin = -1;

				String subject = studentgroup[i].subjectsInGroup[j];

				// looping through every teacher to find which teacher teaches the current subject
				for (int k = 0; k < numberOfTeachers; k++) {

					// if such teacher found,checking if he should be assigned the subject or some other teacher based on prior assignments
					if (teacher[k].subject.equalsIgnoreCase(subject)) {

						// if first teacher found for this subject
						if (assignedmin == -1) {
							assignedmin = teacher[k].assigned;
							teacherid = k;
						}

						// if teacher found has less no of pre assignments than the teacher assigned for this subject
						else if (assignedmin > teacher[k].assigned) {
							assignedmin = teacher[k].assigned;
							teacherid = k;
						}
					}
				}

				// 'assigned' variable for selected teacher incremented
				teacher[teacherid].assigned++;

				studentgroup[i].teacherid[j]= teacherid;
			}
		}
	}
	private static int countNumberOfSubjectsPerSubGroupOrGroup(List<GenericGroupInformation> subjects, String subGroupName) {
		int count = 0;
		for (GenericGroupInformation o : subjects) {
			if (o.getSubGroup().equalsIgnoreCase(subGroupName)) {
				count++;
			}
		}
		return count;
	}

	private static String[] getSubjectsInSubGroupOrGroup(List<GenericGroupInformation> labs, String subGroupName){
		List<String> subjectsInGroup = new ArrayList<>();
		for (GenericGroupInformation o : labs) {
			if (o.getSubGroup().equalsIgnoreCase(subGroupName)) {
				subjectsInGroup.add(o.getSubjectName());
			}
		}
		String[] array = new String[subjectsInGroup.size()];
		return subjectsInGroup.toArray(array);
	}

	private static int[] getArrayHours(List<GenericGroupInformation> generalGroupsInformation, String[] subjectsInGroup, String name){
		int[] requiredNumberOfLessonsInAWeek = new int[subjectsInGroup.length];
		int index;
		for (GenericGroupInformation o : generalGroupsInformation) {
			if (o.getSubGroup().equalsIgnoreCase(name)) {
				index = ArrayUtils.indexOf(subjectsInGroup, o.getSubjectName());
				requiredNumberOfLessonsInAWeek[index] = o.getRequiredLessonsInAWeek();
			}
		}
		return requiredNumberOfLessonsInAWeek;
	}

}