package time_table.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import time_table.converter.row.CharacteristicsRow;
import time_table.converter.sheet.ExcelSheet;
import time_table.domain.base.RowsImporter;
import time_table.service.mapper.ExcelSheetToEntityMapper;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@Slf4j
@RequiredArgsConstructor

@Service
public class CharacteristicsRowsImporter implements RowsImporter<ExcelSheet, CharacteristicsRow> {

    private final ExcelSheetToEntityMapper<CharacteristicsRow, ExcelSheet> characteristicsReflectionMapper;

    @Override
    public List<CharacteristicsRow> importSheet(ExcelSheet excelSheet) throws IOException {
        List<CharacteristicsRow> characteristics = characteristicsReflectionMapper
                .mapSheet(excelSheet, CharacteristicsRow.class);
        log.info("Characteristics tab is: {}", Arrays.toString(characteristics.toArray()));
        return characteristics;
    }

}
