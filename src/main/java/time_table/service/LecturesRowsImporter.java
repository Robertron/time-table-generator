package time_table.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.dhatim.fastexcel.reader.Cell;
import org.dhatim.fastexcel.reader.Row;
import org.dhatim.fastexcel.reader.Sheet;
import org.springframework.stereotype.Service;
import time_table.converter.row.ExcelRow;
import time_table.converter.row.LecturesRow;
import time_table.converter.sheet.ExcelSheet;
import time_table.domain.base.RowsImporter;

import java.io.IOException;
import java.util.*;

@Slf4j
@RequiredArgsConstructor

@Service
public class LecturesRowsImporter implements RowsImporter<ExcelSheet, LecturesRow> {

    @Override
    public List<LecturesRow> importSheet(ExcelSheet excelSheet) throws IOException {
        //Number of non-variable columns
        int numberOfNonVariableColumns = 8;
        // Get the header row to extract column names
        Row headerRow = excelSheet.getSheet().read().get(0);

        // Create a list to store the data rows
        List<LecturesRow> lectures = new ArrayList<>();

        // Create a list to store the column names for "group" columns
        List<String> subGroupColumnNames = new ArrayList<>();

        // Iterate over cells in the header row
        for (int columnIndex = numberOfNonVariableColumns; columnIndex < headerRow.getCellCount(); columnIndex++) {
            Cell headerCell = headerRow.getCell(columnIndex);
            String subGroupName = headerCell.asString();
            subGroupColumnNames.add(subGroupName);
        }

        // Iterate over rows
        for (Row row : excelSheet.getSheet().read()) {
            // Skip the header row
            if (row.getRowNum() == 1) {
                continue;
            }
            if (row.getCellCount() != 0 && row.getFirstNonEmptyCell().isPresent()){
                // Get the values from the row and create a LecturesRow object
                LecturesRow lecture = createLecturesRow(
                        new ExcelRow(excelSheet.getSheet().getIndex(), row),
                        subGroupColumnNames,
                        excelSheet.getSheet()
                );
                lectures.add(lecture);
            }
        }
        log.info("Lectures tab is: {}", Arrays.toString(lectures.toArray()));
        return lectures;
    }

    private static LecturesRow createLecturesRow(ExcelRow row, List<String> subGroupColumnNames, Sheet sheet) throws IOException {
        String group = row.asString(0).replaceAll("\\s", "");
        String subject = row.asString(1);
        String professor = row.asString(2);
        String tutorialTeacher = row.asString(3);
        int numberOfStudents = row.asInteger(4);
        int lecturesPerWeek = row.asInteger(5);
        int tutorialsPerWeek = row.asInteger(6);
        int labsPerWeek = row.asInteger(7);

        // Extract group data dynamically based on the column names
        Map<String, String> subGroups = new HashMap<>();
        for (String columnName : subGroupColumnNames) {
            int columnIndex = getColumnIndex(sheet, columnName);
            if (columnIndex != -1) {
                Cell cell = row.getCell(columnIndex).orElse(null);
                if (cell != null) {
                    String groupValue = cell.asString();
                    subGroups.put(columnName, groupValue);
                }
            }
        }

        // Create and return LecturesRow object
        return new LecturesRow(
                group,
                subject,
                professor,
                tutorialTeacher,
                numberOfStudents,
                lecturesPerWeek,
                tutorialsPerWeek,
                labsPerWeek,
                subGroups
        );
    }

    private static int getColumnIndex(Sheet sheet, String columnName) throws IOException {
        Row headerRow = sheet.read().get(0);
        for (int columnIndex = 0; columnIndex < headerRow.getCellCount(); columnIndex++) {
            Cell headerCell = headerRow.getCell(columnIndex);
            if (headerCell.asString().equalsIgnoreCase(columnName)) {
                return columnIndex;
            }
        }
        return -1;
    }
}
