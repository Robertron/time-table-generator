package time_table.service.mapper;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import time_table.converter.row.ExcelRow;
import time_table.converter.sheet.ExcelSheet;

import java.io.IOException;
import java.util.List;
import java.util.stream.Stream;

@RequiredArgsConstructor

@Service
public class ExcelSheetToEntityMapper<E, S extends ExcelSheet> {

    private final InstanceFromExcelRowReflectionInitializer instanceFromExcelRowReflectionInitializer;

    public List<E> mapSheet(S excelSheet, Class<E> entityClass) throws IOException {
        return this.getEntities(excelSheet, entityClass)
                .toList();
    }

    private Stream<E> getEntities(S excelSheet, Class<E> entityClass) throws IOException {
        return excelSheet.getSheet().openStream()
                .skip(excelSheet.getHeaderSize())
                .filter(row -> row.getCellCount() != 0 && row.getFirstNonEmptyCell().isPresent())
                .map(row -> instanceFromExcelRowReflectionInitializer.getInstance(
                        new ExcelRow(excelSheet.getSheet().getIndex(), row),
                        entityClass
                ));
    }

}
