package time_table.service.mapper;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import time_table.converter.cell.Position;
import time_table.converter.row.ExcelRow;
import time_table.exception.ExcelCellReaderException;
import time_table.util.DateConverter;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;

@RequiredArgsConstructor
@Slf4j
@Service
public class InstanceFromExcelRowReflectionInitializer {

    public <T> T getInstance(ExcelRow excelRow, Class<T> rowClass) {
        T instance;
        try {
            instance = rowClass.getConstructor().newInstance();
            Field[] fields = rowClass.getDeclaredFields();
            for (Field field : fields) {
                field.setAccessible(true);
                Position positionAnnotation = field.getAnnotation(Position.class);
                if (positionAnnotation != null) {
                    int position = positionAnnotation.position();
                    try {
                        if (field.getType().isAssignableFrom(Long.class)) {
                            field.set(instance, excelRow.asLong(position));
                        }
                        if (field.getType().isAssignableFrom(String.class)) {
                            field.set(instance, excelRow.toString(position));
                        }
                        if (field.getType().isAssignableFrom(LocalDate.class)) {
                            field.set(instance, DateConverter.fromStringToLocalDate(excelRow.asString(position)));
                        }
                        if (field.getType().isAssignableFrom(BigDecimal.class)) {
                            field.set(instance, excelRow.asBigDecimal(position));
                        }
                        if (field.getType().isAssignableFrom(Integer.class)) {
                            field.set(instance, excelRow.asInteger(position));
                        }
                        if (field.getType().isAssignableFrom(LocalTime.class)) {
                            field.set(instance, DateConverter.fromBigDecimalToLocalTime(excelRow.asBigDecimal(position)));
                        }
                    } catch (ExcelCellReaderException e) {
                        log.error(e.getMessage());
                        throw e;
                    }

                    field.setAccessible(false);
                }
            }
        } catch (NoSuchMethodException | InstantiationException | IllegalAccessException |
                 InvocationTargetException e) {
            throw new RuntimeException(e);
        }

        return instance;
    }

}
