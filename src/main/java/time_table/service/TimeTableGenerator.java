package time_table.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import time_table.domain.dto.TimeTableDto;
import time_table.exception.ExcelFileExpectedException;
import time_table.external.messaging.FileProcessingProducer;
import time_table.external.messaging.dto.ExcelFileProcessingMessage;

import java.io.File;
import java.util.List;

import static time_table.util.FileHelper.getFile;
import static time_table.util.FileHelper.isExcel;

@Slf4j
@RequiredArgsConstructor
@Service
public class TimeTableGenerator {
    private final FileProcessingProducer fileProcessingProducer;;

    public  List<TimeTableDto> runGenerator() {

        String path = "teaching-allocation-v.2.xlsx";
        String template = "schedule-template";

        File file = getFile(path);
        if (!isExcel(file)) {
            throw new ExcelFileExpectedException();
        }

        return fileProcessingProducer.sendAndReceive(new ExcelFileProcessingMessage(//https://www.rabbitmq.com/tutorials/tutorial-six-spring-amqp.html
                path,
                file.length(),
                template
        ));
    }
}
