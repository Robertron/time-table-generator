package time_table.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import time_table.converter.row.SubGroupsRow;
import time_table.converter.sheet.ExcelSheet;
import time_table.domain.base.RowsImporter;
import time_table.service.mapper.ExcelSheetToEntityMapper;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
@Slf4j
@RequiredArgsConstructor
@Service
public class SubGroupsRowsImporter implements RowsImporter<ExcelSheet, SubGroupsRow> {

    private final ExcelSheetToEntityMapper<SubGroupsRow, ExcelSheet> subjectsReflectionMapper;

    @Override
    public List<SubGroupsRow> importSheet(ExcelSheet surveySheet) throws IOException {
        List<SubGroupsRow> subGroups = subjectsReflectionMapper
                .mapSheet(surveySheet, SubGroupsRow.class);
        log.info("SubGroups tab is: {}", Arrays.toString(subGroups.toArray()));
        return subGroups;
    }
}
