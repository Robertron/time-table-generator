package time_table.converter.sheet;

import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class ScheduleSheetRepository {

    public static final String SUBGROUPS = "subgroups";
    public static final String LECTURES = "lectures";
    public static final String CHARACTERISTICS = "characteristics";

    private static final Set<String> SCHEDULE_SHEET_NAMES = Set.of(
            SUBGROUPS,
            LECTURES,
            CHARACTERISTICS
    );

    public Set<String> getSheetNames() {
        return SCHEDULE_SHEET_NAMES;
    }

    public int getSheetsCount() {
        return SCHEDULE_SHEET_NAMES.size();
    }
}
