package time_table.converter.sheet;

import lombok.Getter;
import lombok.Setter;
import org.dhatim.fastexcel.reader.Sheet;

@Getter
@Setter
public class ExcelSheet {
    private final Sheet sheet;
    private final int headerSize;

    public ExcelSheet(
            Sheet sheet,
            int headerSize
    ) {
        this.sheet = sheet;
        this.headerSize = headerSize;
    }

    public String getName() {
        return sheet.getName();
    }
}