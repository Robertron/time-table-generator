package time_table.converter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import time_table.converter.row.CharacteristicsRow;
import time_table.converter.row.LecturesRow;
import time_table.converter.row.SubGroupsRow;
import time_table.converter.sheet.ExcelSheet;
import time_table.domain.base.RowsImporter;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static time_table.converter.sheet.ScheduleSheetRepository.*;
import static time_table.domain.Templates.SCHEDULE_TEMPLATE;

@Slf4j

@Service
public class ExcelDocumentRowsImporter {

    private final Map<String, Map<String, RowsImporter<? super ExcelSheet, ?>>> allImporters;

    public ExcelDocumentRowsImporter(
            @Qualifier("subGroupsRowsImporter") RowsImporter<ExcelSheet, SubGroupsRow> labsSheetRowsImporter,
            @Qualifier("lecturesRowsImporter") RowsImporter<ExcelSheet, LecturesRow> lecturesSheetRowsImporter,
            @Qualifier("characteristicsRowsImporter") RowsImporter<ExcelSheet, CharacteristicsRow> characteristicsSheetRowsImporter
    ) {
        allImporters = new HashMap<>();

        //Teaching allocation document
        Map<String, RowsImporter<? super ExcelSheet, ?>> scheduleImporters = Map.of(
                SUBGROUPS, labsSheetRowsImporter,
                LECTURES, lecturesSheetRowsImporter,
                CHARACTERISTICS, characteristicsSheetRowsImporter
        );
        allImporters.put(SCHEDULE_TEMPLATE.name(), scheduleImporters);
    }
    public List<?> importRows(String template, ExcelSheet sheet) throws IOException {
        Map<String, RowsImporter<? super ExcelSheet, ?>> importersByTemplate = allImporters.get(template);

        RowsImporter<? super ExcelSheet, ?> importer = importersByTemplate.get(sheet.getName());

        if (importer != null) {
            log.info("<< Excel sheet processing started in the tab - {}", sheet.getName());
            return importer.importSheet(sheet);
        }
        return List.of();
    }
}
