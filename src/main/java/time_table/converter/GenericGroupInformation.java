package time_table.converter;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class GenericGroupInformation {

    /**
     * Student group
     */
    private String group;

    /**
     * Student subgroup
     */
    private String subGroup;

    /**
     * Lab of a subject
     */
    private String subjectName;

    /**
     * Course assistance
     */
    private String instructor;

    /**
     * № of students in a subject
     */
    private Integer numberOfStudents;

    /**
     * № of lessons that a subject must be taught in a week
     */
    private Integer requiredLessonsInAWeek;

}
