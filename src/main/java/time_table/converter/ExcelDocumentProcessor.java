package time_table.converter;

import lombok.extern.slf4j.Slf4j;
import org.dhatim.fastexcel.reader.ReadableWorkbook;
import org.dhatim.fastexcel.reader.Sheet;
import org.springframework.stereotype.Service;
import time_table.converter.row.LecturesRow;
import time_table.converter.sheet.ExcelSheet;
import time_table.service.LecturesRowsImporter;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static time_table.converter.sheet.ScheduleSheetRepository.*;
import static time_table.domain.Templates.SCHEDULE_TEMPLATE;

@Slf4j
@Service
public class ExcelDocumentProcessor {
    private final ExcelDocumentRowsImporter excelDocumentRowsImporter;

    public ExcelDocumentProcessor(
            ExcelDocumentRowsImporter excelDocumentRowsImporter
    ) {
        this.excelDocumentRowsImporter = excelDocumentRowsImporter;

    }
    public ArrayList<List<?>> processDoc(String template, File file) throws IOException {
        LecturesRowsImporter lecturesRowsImporter = new LecturesRowsImporter();
        ArrayList<List<?>> tabsList = new ArrayList<>(3);
        List<LecturesRow> allLectures = new ArrayList<>();
        log.info(">> Excel document processing");
        try (ReadableWorkbook workbook = new ReadableWorkbook(file)) {
            for (Sheet sheet : workbook.getSheets().toList()) {
                ExcelSheet excelSheet = new ExcelSheet(
                        sheet,
                        getHeaderSize(template, sheet.getName())
                );
                if (excelSheet.getName().startsWith("lectures")){
                    allLectures.addAll(lecturesRowsImporter.importSheet(excelSheet));
                } else {
                    tabsList.add(excelDocumentRowsImporter.importRows(template, excelSheet));
                }
            }
            tabsList.add(allLectures);
            return tabsList;
        }
    }

    private static int getHeaderSize(String template, String sheetName) {
        Map<String, Map<String, Integer>> headerSizes = Map.of(
                SCHEDULE_TEMPLATE.name(), Map.of(
                        SUBGROUPS, 1,
                        LECTURES, 0,
                        CHARACTERISTICS, 1
                )
        );
        return headerSizes.get(template).getOrDefault(sheetName, 1);
    }
}
