package time_table.converter.row;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import time_table.converter.cell.Position;

import java.time.LocalTime;
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)

@Data
//@AllArgsConstructor
public class CharacteristicsRow {

    /**
     * Number of time periods that there will be in one study day
     */
    @Position(position = 0)
    private Integer timePeriodsPerDay;

    /**
     * № of study days in a week
     */
    @Position(position = 1)
    private Integer daysPerWeek;

    /**
     * Room code
     */
    @Position(position = 2)
    private Integer roomCode;

    /**
     * Room capacity expressed in number of available seats in a room
     */
    @Position(position = 3)
    private Integer roomCapacity;

    /**
     * Starting time of a study period in a given day
     */
    @Position(position = 4)
    private LocalTime periodStartTime;

    /**
     * Ending time of a study period in a given day
     */
    @Position(position = 5)
    private LocalTime periodEndTime;
}
