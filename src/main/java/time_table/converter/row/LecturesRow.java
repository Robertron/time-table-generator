package time_table.converter.row;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)

@Data
@AllArgsConstructor
public class LecturesRow {

    /**
     * Student group
     */
    private String group;

    /**
     * Lecture of a year
     */
    private String subject;

    /**
     * Course professor
     */
    private String professor;

    /**
     * Course professor
     */
    private String tutorialTeacher;

    /**
     * № of students in a subject
     */
    private Integer numberOfStudentsInLecturesAndTutorials;

    /**
     * № of periods that a lecture must be taught in a week
     */
    private Integer lecturesInAWeek;

    /**
     * № of periods that a tutorial must be taught in a week
     */
    private Integer tutorialsInAWeek;

    /**
     * № of periods that a lab must be taught in a week
     */
    private Integer labsPerWeek;

    /**
     * SubGroups information Map<SubGroupName, teachingAssistantName>
     */
    private Map<String, String> subGroups;

}
