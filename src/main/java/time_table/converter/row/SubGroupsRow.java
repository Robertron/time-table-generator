package time_table.converter.row;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import time_table.converter.cell.Position;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)

@Data
public class SubGroupsRow {

    /**
     * Student group
     */
    @Position(position = 0)
    private String group;

    /**
     * Student subgroup
     */
    @Position(position = 1)
    private String subGroup;

    /**
     * № of students in a subject
     */
    @Position(position = 2)
    private Integer numberOfStudents;

}
