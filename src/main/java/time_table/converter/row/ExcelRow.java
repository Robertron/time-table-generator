package time_table.converter.row;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.dhatim.fastexcel.reader.Cell;
import org.dhatim.fastexcel.reader.CellType;
import org.dhatim.fastexcel.reader.ExcelReaderException;
import org.dhatim.fastexcel.reader.Row;
import time_table.exception.ExcelCellReaderException;

import java.math.BigDecimal;
import java.util.Optional;

@Slf4j
public class ExcelRow {
    private Integer sheetNumber;
    private final Row row;

    public ExcelRow(
            Row row
    ) {
        this.row = row;
    }

    public ExcelRow(Integer sheetNumber, Row row) {
        this.sheetNumber = sheetNumber;
        this.row = row;
    }

    public Integer getSheetNumber() {
        return this.sheetNumber;
    }

    public Integer getRowNum() {
        return this.row.getRowNum();
    }

    public static ExcelRow wrapper(
            Integer sheetNumber,
            Row row
    ) {
        return new ExcelRow(
                sheetNumber,
                row
        );
    }

    public Long asLong(int colNum) {
        return this.getCell(colNum)
                .flatMap(value -> getValue(value).map(BigDecimal::longValue))
                .orElse(null);
    }

    public BigDecimal asBigDecimal(int colNum) {
        return this.getCell(colNum)
                .flatMap(ExcelRow::getValue)
                .orElse(null);
    }

    public Integer asInteger(int colNum) {
        return this.getCell(colNum)
                .flatMap(value -> getValue(value).map(BigDecimal::intValue))
                .orElse(null);
    }

    public Double asDouble(int colNum) {
        return this.getCell(colNum)
                .flatMap(value -> getValue(value).map(BigDecimal::doubleValue))
                .orElse(null);
    }

    public String toString(int colNum) {
        Optional<Cell> cellOpt = this.getCell(colNum);

        if (cellOpt.isPresent()) {
            Cell cell = cellOpt.get();
            if (cell.getValue() != null) {
                if (cell.getValue().equals("-")) {
                    return null;
                }
            }
            String val = cell.getText();
            return StringUtils.isNotBlank(val) ? val : null;
        }

        return null;
    }

    public String asString(int colNum) {
        try {
            Optional<Cell> cellOpt = this.getCell(colNum);

            if (cellOpt.isPresent()) {
                Cell cell = cellOpt.get();
                if (cell.getType().equals(CellType.FORMULA)) {
                    return cell.getFormula();
                }

                return cell.asString();
            }
        } catch (ExcelReaderException e) {
            throw new ExcelCellReaderException(e.getMessage(), colNum);
        }

        return null;
    }

    public Optional<Cell> getCell(int colNum) {
        Optional<Cell> cell = Optional.empty();
        try {
            cell = Optional.ofNullable(row.getCell(colNum));
        } catch (IndexOutOfBoundsException e) {
            log.error("Accessing a non-existent cell by index - {}", colNum);
        }
        return cell;
    }

    private static Optional<BigDecimal> getValue(Cell cell) {
        try {
            BigDecimal value;
            if (cell.getValue() != null) {
                if (cell.getValue().equals("-")) {
                    return Optional.empty();
                }
            }
            if (cell.getType().equals(CellType.FORMULA)) {
                if (cell.getValue() instanceof BigDecimal bigDecimalValue) {
                    value = bigDecimalValue;
                } else {
                    value = BigDecimal.ZERO;
                }
            } else if (cell.getType().equals(CellType.STRING)) {
                try {
                    value = new BigDecimal(cell.asString());
                } catch (NumberFormatException e) {
                    value = null;
                }
            } else {
                value = cell.asNumber();
            }
            return Optional.ofNullable(value);
        } catch (ExcelReaderException e) {
            throw new ExcelCellReaderException(e.getMessage(), cell.getColumnIndex());
        }
    }
}