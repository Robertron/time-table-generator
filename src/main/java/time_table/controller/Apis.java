package time_table.controller;

public final class Apis {

    /**
     * Root address
     */
    public static final String BASE_API = "/schedule";

    /**
     * Generate Time table
     */
    public static final String GENERATE = "/generate";

    /**
     * Terms of Service
     */
    public static final String TERMS_OF_SERVICE = "/terms-of-service";
}
