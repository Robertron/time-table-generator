package time_table.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.web.bind.annotation.*;
import time_table.domain.dto.TimeTableDto;
import time_table.service.TimeTableGenerator;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;

import static time_table.controller.Apis.*;

@Tag(name = "Time-table Schedule Generator")

@Slf4j
@RequiredArgsConstructor
@RestController
@CrossOrigin("http://localhost:8080")
@RequestMapping(BASE_API)
public class GenerateTimeTableController {

    private final TimeTableGenerator timeTableGenerator;
    private final ResourceLoader resourceLoader;
    @Operation(summary = "Generate time table")
    @GetMapping(GENERATE)
    public  List<TimeTableDto> getGeneratedTimeTable() {
        log.info(">> Request for time-table generation");
        return timeTableGenerator.runGenerator();
    }

    @Operation(summary = "Go to terms of service")
    @GetMapping(TERMS_OF_SERVICE)
    @ResponseBody
    public String obtainTermsOfService() throws IOException {//https://stackoverflow.com/a/72472953
        String filePath = "classpath:templates/terms-of-service.html";
        Resource resource = resourceLoader.getResource(filePath);
        InputStream inputStream = resource.getInputStream();
        String fileContents = new String(inputStream.readAllBytes(), StandardCharsets.UTF_8);
        inputStream.close();
        return fileContents;
    }

}
