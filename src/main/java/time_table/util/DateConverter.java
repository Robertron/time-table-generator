package time_table.util;

import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public final class DateConverter {

    private static final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
    public static LocalDate fromStringToLocalDate(String date) {
        if (StringUtils.isBlank(date)) {
            return null;
        }
        return LocalDate.parse(date, dateFormatter);
    }
    public static LocalTime fromBigDecimalToLocalTime(BigDecimal timeValue) {
        if (timeValue == null){
            return null;
        }
        int hours = timeValue.multiply(BigDecimal.valueOf(24)).intValue();
        int minutes = timeValue.multiply(BigDecimal.valueOf(24 * 60))
                .remainder(BigDecimal.valueOf(60)).intValue();

        return LocalTime.of(hours, minutes);
    }
}
