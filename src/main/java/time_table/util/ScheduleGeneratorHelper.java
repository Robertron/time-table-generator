package time_table.util;

import lombok.extern.slf4j.Slf4j;
import time_table.Generator.Room;
import time_table.converter.GenericGroupInformation;
import time_table.converter.row.CharacteristicsRow;
import time_table.converter.row.LecturesRow;
import time_table.converter.row.SubGroupsRow;
import time_table.domain.data_arrangements.GroupTable;
import time_table.domain.data_arrangements.GroupWithSubGroupsTable;
import time_table.domain.dto.SubGroupTableDto;
import time_table.domain.dto.TimeTableDto;

import java.util.*;

@Slf4j
public class ScheduleGeneratorHelper {
    public static List<GenericGroupInformation> mapToSubGroupsInformation(List<SubGroupsRow> subGroups, List<LecturesRow> lectures){
        List<GenericGroupInformation> subGroupsInformation = new ArrayList<>();
        for(LecturesRow lecture: lectures){
            if (lecture.getLabsPerWeek() > 0) {
                Map<String, String> subGroupsAndAssistants = lecture.getSubGroups();

                int subGroupCounter = 0;
                for (Map.Entry<String, String> entry : subGroupsAndAssistants.entrySet()) {

                    String subGroupName = entry.getKey();
                    String assistantName = entry.getValue();
                    String groupName = lecture.getGroup();
                    String subjectName = lecture.getSubject();
                    Integer labsPerWeek = lecture.getLabsPerWeek();

                    GenericGroupInformation subGroupInformation = GenericGroupInformation.builder()
                            .group(groupName)
                            .subGroup(subGroupName)
                            .subjectName(subjectName + " - (LAB)")
                            .instructor(assignAssistantForLabsWithSeveralAssistantOptions(assistantName, subjectName, subGroupCounter, subGroupName))
                            .numberOfStudents(getNumberOfStudentsPerSubGroup(subGroups, subGroupName, groupName))
                            .requiredLessonsInAWeek(labsPerWeek)
                            .build();

                    subGroupsInformation.add(subGroupInformation);
                    subGroupCounter++;
                }
            }
        }
        return subGroupsInformation;
    }

    //Assigns assistant when there is more than one assistant per subgroup
    private static String assignAssistantForLabsWithSeveralAssistantOptions(String assistantName, String subjectName, int subGroupCounter, String subGroupName){
        if (assistantName.toLowerCase(Locale.ROOT).contains(",")){
            String[] stringArrayOfAssistants = assistantName.split(",\\s*"); // Split the string by comma and optional whitespace
            List<String> assistantsList = Arrays.asList(stringArrayOfAssistants); // Convert the array to a list
            int index = subGroupCounter % assistantsList.size();
            String assistant = assistantsList.get(index);
            log.info( String.format("The assistant for the subgroup - %s, " +
                    "and the subject - %s is %s", subGroupName, subjectName, assistant
                    )
            );
            return assistant;
        } else {
            return assistantName;
        }
    }

    public static List<GenericGroupInformation> mapToGroupsInformation(List<LecturesRow> lecturesForGroupSchedule){
        List<GenericGroupInformation> groupsInformation = new ArrayList<>();
        for (LecturesRow lecture : lecturesForGroupSchedule) {
            if (lecture.getLecturesInAWeek() > 0) {
                GenericGroupInformation group = GenericGroupInformation.builder()
                        .group(lecture.getGroup().replaceAll("\\s", ""))
                        .subGroup(lecture.getGroup().replaceAll("\\s", ""))
                        .subjectName(lecture.getSubject().toLowerCase(Locale.ROOT).contains("physical") ? lecture.getSubject() : lecture.getSubject() + " - (LEC)")
                        .instructor(lecture.getProfessor())
                        .numberOfStudents(lecture.getNumberOfStudentsInLecturesAndTutorials())
                        .requiredLessonsInAWeek(lecture.getLecturesInAWeek())
                        .build();

                groupsInformation.add(group);
            }
            if (lecture.getTutorialsInAWeek() > 0) {
                GenericGroupInformation group = GenericGroupInformation.builder()
                        .group(lecture.getGroup().replaceAll("\\s", ""))
                        .subGroup(lecture.getGroup().replaceAll("\\s", ""))
                        .subjectName(lecture.getSubject().toLowerCase(Locale.ROOT).contains("physical") ? lecture.getSubject() : lecture.getSubject() + " - (TUT)")
                        .instructor(lecture.getTutorialTeacher())
                        .numberOfStudents(lecture.getNumberOfStudentsInLecturesAndTutorials())
                        .requiredLessonsInAWeek(lecture.getTutorialsInAWeek())
                        .build();

                groupsInformation.add(group);
            }

        }
        return groupsInformation;
    }

    public static int getNumberOfStudentsPerSubGroup(List<SubGroupsRow> subGroups, String subGroupName, String groupName){
        Optional<Integer> numberOfStudentsPerSubGroup = subGroups.stream()
                .filter(
                        a -> groupName.replaceAll("\\s", "").equalsIgnoreCase(a.getGroup().replaceAll("\\s", ""))
                                && subGroupName.replaceAll("\\s", "").equalsIgnoreCase(a.getSubGroup().replaceAll("\\s", ""))
                )
                .map(SubGroupsRow::getNumberOfStudents)
                .findFirst();
        return numberOfStudentsPerSubGroup.orElseThrow(
                () -> new IllegalArgumentException(
                        "The number of students for group " + groupName + "and subGroup " + subGroupName + "was not given!"
                )
        );
    }

    public static List<GroupWithSubGroupsTable> assignGroupToSubGroupTimeTable(
            ArrayList<List<?>> tabsList,
            List<SubGroupTableDto> subGroupTimeTable
    ) {
        List<GroupWithSubGroupsTable> groupWithSubGroupsTableList = new ArrayList<>();
        // Find the list of lab objects
        List<SubGroupsRow> labs = getRowList(tabsList, SubGroupsRow.class);

        // Find the list of group and subGroup names Map<groupName, Set<subGroupName>> groupList
        Map<String, Set<String>> groupList = new HashMap<>();
        for (SubGroupsRow lab : labs) {
            String groupName = lab.getGroup().replaceAll("\\s", "");
            String subGroupName = lab.getSubGroup().replaceAll("\\s", "");
            groupList.computeIfAbsent(groupName, k -> new HashSet<>()).add(subGroupName);
        }

        //Match group to subGroup timeTable
        for (Map.Entry<String, Set<String>> entry : groupList.entrySet()) {

            String groupName = entry.getKey();
            Set<String> subGroupNames = entry.getValue();

            GroupWithSubGroupsTable groupTable = new GroupWithSubGroupsTable();
            List<SubGroupTableDto> subGroupTableDtoList = new ArrayList<>();

            for (String subGroupName : subGroupNames) {
                for (SubGroupTableDto subGroupTableDto : subGroupTimeTable) {
                    if (subGroupTableDto.getSubGroup().replaceAll("\\s", "").equalsIgnoreCase(subGroupName)) {
                        subGroupTableDtoList.add(subGroupTableDto);
                    }
                }
            }

            groupTable.setGroup(groupName);
            groupTable.setSubGroupsTable(subGroupTableDtoList);

            groupWithSubGroupsTableList.add(groupTable);
        }

        return groupWithSubGroupsTableList;

    }

    //Get the room code and room capacity from the characteristics tab
    public static List<Room> getRoomInformation(List<CharacteristicsRow> characteristics){
        return characteristics.stream()
                .map(characteristicsRow -> new Room(characteristicsRow.getRoomCode(), characteristicsRow.getRoomCapacity()))
                .toList();
    }

    //Get the list type from the Excel tabsList
    public static <T> List<T> getRowList(ArrayList<List<?>> tabsList, Class<T> clazz){
        return tabsList.stream()
                .filter(list -> !list.isEmpty() && clazz.isAssignableFrom(list.get(0).getClass()))
                .findFirst()
                .map(list -> castList(list, clazz))
                .orElse(null);
    }

    // Helper method to safely cast lists to the desired type
    private static <T> List<T> castList(List<?> list, Class<T> clazz) {
        List<T> result = new ArrayList<>(list.size());
        for (Object obj : list) {
            result.add(clazz.cast(obj));
        }
        return result;
    }

    public static int calculatePeriodsForLecturesAndTutorials(List<LecturesRow> lectures, List<CharacteristicsRow> characteristics) {

        int numberOfRequiredTimeSlotsForLecturesAndTutorials = findMaxSumOfLecturesAndTutorialsInGroup(lectures);

        int daysOfStudyPerWeek = characteristics.get(0).getDaysPerWeek();

        int periodsForLecturesAndTutorials = 1;

        if (numberOfRequiredTimeSlotsForLecturesAndTutorials > daysOfStudyPerWeek) {
            periodsForLecturesAndTutorials = 2;
        }
        if (numberOfRequiredTimeSlotsForLecturesAndTutorials > daysOfStudyPerWeek * 2) {
            periodsForLecturesAndTutorials = 3;
        }
        if (numberOfRequiredTimeSlotsForLecturesAndTutorials > daysOfStudyPerWeek * 3) {
            periodsForLecturesAndTutorials = 4;
        }

        return periodsForLecturesAndTutorials;
    }

    public static int findMaxSumOfLecturesAndTutorialsInGroup(List<LecturesRow> lectures) {
        Map<String, Integer> groupSums = new HashMap<>();

        for (LecturesRow lecture : lectures) {
            String group = lecture.getGroup();
            Integer sum = groupSums.getOrDefault(group, 0) + lecture.getLecturesInAWeek() + lecture.getTutorialsInAWeek();
            groupSums.put(group, sum);
        }

        int maxSum = Integer.MIN_VALUE;
        for (Integer sum : groupSums.values()) {
            if (sum > maxSum) {
                maxSum = sum;
            }
        }

        return maxSum;
    }

    public static List<TimeTableDto> joinTimeTableGenerations(
            List<GroupTable> groupTimeTable,
            List<GroupWithSubGroupsTable> groupWithSubGroups
    ) {
        List<TimeTableDto> timeTableDtoList = new ArrayList<>();

        for (GroupWithSubGroupsTable groupWithSubGroupsTable : groupWithSubGroups) {

            TimeTableDto timetable = new TimeTableDto();

            String groupName = groupWithSubGroupsTable.getGroup();

            List<SubGroupTableDto> subGroupsTable = groupWithSubGroupsTable.getSubGroupsTable();

            timetable.setGroup(groupName);

            for (GroupTable groupTable : groupTimeTable) {
                if (groupTable.getGroup().replaceAll("\\s", "").equalsIgnoreCase(groupName)) {
                    timetable.setGroupStudyDtoList(groupTable.getGroupStudyDtoList());
                }
            }

            timetable.setSubGroupsTable(subGroupsTable);

            timeTableDtoList.add(timetable);
        }

        return timeTableDtoList;
    }

}
