package time_table.util;

import org.apache.commons.io.FilenameUtils;
import time_table.exception.ExcelFileNotFoundException;

import java.io.File;

public final class FileHelper {

    public static File getFile(String path) {
        File file = new File(path);
        if (!file.exists()) {
            throw new ExcelFileNotFoundException(String.format("File with the path given - %s was not found", path));
        }
        return file;
    }

    public static boolean isExcel(File file) {
        return FilenameUtils.getExtension(file.getName())
                .equalsIgnoreCase("xlsx");
    }

}
