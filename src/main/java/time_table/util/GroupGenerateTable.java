package time_table.util;

import time_table.converter.GenericGroupInformation;
import time_table.converter.row.CharacteristicsRow;
import time_table.domain.data_arrangements.GroupTable;
import time_table.domain.dto.GroupPeriodDto;
import time_table.domain.dto.GroupStudyDto;

import java.util.ArrayList;
import java.util.List;

/**
 * It generates a mocked group timetable, for testing purposes.
 */
public class GroupGenerateTable {

    public static List<GroupTable> mock(List<GenericGroupInformation> groupsInformation, List<CharacteristicsRow> characteristics) {
        List<GroupTable> groupTables = new ArrayList<>();

        GroupTable groupTable = new GroupTable();

        List<GroupStudyDto> groupStudyDtos = new ArrayList<>();

        //MONDAY
        GroupStudyDto groupStudyDto = new GroupStudyDto();

        List<GroupPeriodDto> groupPeriodDtos = new ArrayList<>();

        GroupPeriodDto groupPeriodDto = new GroupPeriodDto();
        groupPeriodDto.setPeriod("Period " + (1));
        groupPeriodDto.setStartTime(characteristics.get(0).getPeriodStartTime());
        groupPeriodDto.setEndTime(characteristics.get(0).getPeriodEndTime());
        groupPeriodDto.setSubject(groupsInformation.get(0).getSubjectName());
        groupPeriodDto.setNumberOfStudents(groupsInformation.get(0).getNumberOfStudents());
        groupPeriodDto.setInstructorName(groupsInformation.get(0).getInstructor());
        groupPeriodDto.setRoom(105);
        groupPeriodDtos.add(groupPeriodDto);

        GroupPeriodDto groupPeriodDtoTwo = new GroupPeriodDto();
        groupPeriodDtoTwo.setPeriod("Period " + (2));
        groupPeriodDtoTwo.setStartTime(characteristics.get(1).getPeriodStartTime());
        groupPeriodDtoTwo.setEndTime(characteristics.get(1).getPeriodEndTime());
        groupPeriodDtoTwo.setSubject(groupsInformation.get(1).getSubjectName());
        groupPeriodDtoTwo.setNumberOfStudents(groupsInformation.get(1).getNumberOfStudents());
        groupPeriodDtoTwo.setInstructorName(groupsInformation.get(1).getInstructor());
        groupPeriodDtoTwo.setRoom(106);
        groupPeriodDtos.add(groupPeriodDtoTwo);

        GroupPeriodDto groupPeriodDtoThree = new GroupPeriodDto();
        groupPeriodDtoThree.setPeriod("Period " + (3));
        groupPeriodDtoThree.setStartTime(characteristics.get(2).getPeriodStartTime());
        groupPeriodDtoThree.setEndTime(characteristics.get(2).getPeriodEndTime());
        groupPeriodDtoThree.setSubject(groupsInformation.get(2).getSubjectName());
        groupPeriodDtoThree.setNumberOfStudents(groupsInformation.get(2).getNumberOfStudents());
        groupPeriodDtoThree.setInstructorName(groupsInformation.get(2).getInstructor());
        groupPeriodDtoThree.setRoom(108);
        groupPeriodDtos.add(groupPeriodDtoThree);


        groupStudyDto.setDay("MONDAY");
        groupStudyDto.setGroupPeriodDtos(groupPeriodDtos);

        groupStudyDtos.add(groupStudyDto);

        //TUESDAY
        GroupStudyDto groupStudyDtoTwo = new GroupStudyDto();

        List<GroupPeriodDto> groupPeriodDtosTwo = new ArrayList<>();

        GroupPeriodDto groupPeriodDtoTwo1 = new GroupPeriodDto();
        groupPeriodDtoTwo1.setPeriod("Period " + (1));
        groupPeriodDtoTwo1.setStartTime(characteristics.get(0).getPeriodStartTime());
        groupPeriodDtoTwo1.setEndTime(characteristics.get(0).getPeriodEndTime());
        groupPeriodDtoTwo1.setSubject(groupsInformation.get(3).getSubjectName());
        groupPeriodDtoTwo1.setNumberOfStudents(groupsInformation.get(3).getNumberOfStudents());
        groupPeriodDtoTwo1.setInstructorName(groupsInformation.get(3).getInstructor());
        groupPeriodDtoTwo1.setRoom(106);
        groupPeriodDtosTwo.add(groupPeriodDtoTwo1);

        GroupPeriodDto groupPeriodDtoTwo2 = new GroupPeriodDto();
        groupPeriodDtoTwo2.setPeriod("Period " + (2));
        groupPeriodDtoTwo2.setStartTime(characteristics.get(1).getPeriodStartTime());
        groupPeriodDtoTwo2.setEndTime(characteristics.get(1).getPeriodEndTime());
        groupPeriodDtoTwo2.setSubject(groupsInformation.get(4).getSubjectName());
        groupPeriodDtoTwo2.setNumberOfStudents(groupsInformation.get(4).getNumberOfStudents());
        groupPeriodDtoTwo2.setInstructorName(groupsInformation.get(4).getInstructor());
        groupPeriodDtoTwo2.setRoom(108);
        groupPeriodDtosTwo.add(groupPeriodDtoTwo2);

        groupStudyDtoTwo.setDay("TUESDAY");
        groupStudyDtoTwo.setGroupPeriodDtos(groupPeriodDtosTwo);

        groupStudyDtos.add(groupStudyDtoTwo);

        //WEDNESDAY
        GroupStudyDto groupStudyDtoThree = new GroupStudyDto();

        List<GroupPeriodDto> groupPeriodDtosThree = new ArrayList<>();

        GroupPeriodDto groupPeriodDtoThree1 = new GroupPeriodDto();
        groupPeriodDtoThree1.setPeriod("Period " + (1));
        groupPeriodDtoThree1.setStartTime(characteristics.get(0).getPeriodStartTime());
        groupPeriodDtoThree1.setEndTime(characteristics.get(0).getPeriodEndTime());
        groupPeriodDtoThree1.setSubject(groupsInformation.get(5).getSubjectName());
        groupPeriodDtoThree1.setNumberOfStudents(groupsInformation.get(5).getNumberOfStudents());
        groupPeriodDtoThree1.setInstructorName(groupsInformation.get(5).getInstructor());
        groupPeriodDtoThree1.setRoom(106);
        groupPeriodDtosThree.add(groupPeriodDtoThree1);

        GroupPeriodDto groupPeriodDtoThree2 = new GroupPeriodDto();
        groupPeriodDtoThree2.setPeriod("Period " + (2));
        groupPeriodDtoThree2.setStartTime(characteristics.get(1).getPeriodStartTime());
        groupPeriodDtoThree2.setEndTime(characteristics.get(1).getPeriodEndTime());
        groupPeriodDtoThree2.setSubject(groupsInformation.get(6).getSubjectName());
        groupPeriodDtoThree2.setNumberOfStudents(groupsInformation.get(6).getNumberOfStudents());
        groupPeriodDtoThree2.setInstructorName(groupsInformation.get(6).getInstructor());
        groupPeriodDtoThree2.setRoom(106);
        groupPeriodDtosThree.add(groupPeriodDtoThree2);

        groupStudyDtoThree.setDay("WEDNESDAY");
        groupStudyDtoThree.setGroupPeriodDtos(groupPeriodDtosThree);

        groupStudyDtos.add(groupStudyDtoThree);

        //THURSDAY
        GroupStudyDto groupStudyDtoFour = new GroupStudyDto();

        List<GroupPeriodDto> groupPeriodDtosFour = new ArrayList<>();

        GroupPeriodDto groupPeriodDtoFour1 = new GroupPeriodDto();
        groupPeriodDtoFour1.setPeriod("Period " + (1));
        groupPeriodDtoFour1.setStartTime(characteristics.get(0).getPeriodStartTime());
        groupPeriodDtoFour1.setEndTime(characteristics.get(0).getPeriodEndTime());
        groupPeriodDtoFour1.setSubject(groupsInformation.get(7).getSubjectName());
        groupPeriodDtoFour1.setNumberOfStudents(groupsInformation.get(7).getNumberOfStudents());
        groupPeriodDtoFour1.setInstructorName(groupsInformation.get(7).getInstructor());
        groupPeriodDtoFour1.setRoom(107);
        groupPeriodDtosFour.add(groupPeriodDtoFour1);

        GroupPeriodDto groupPeriodDtoFour2 = new GroupPeriodDto();
        groupPeriodDtoFour2.setPeriod("Period " + (2));
        groupPeriodDtoFour2.setStartTime(characteristics.get(1).getPeriodStartTime());
        groupPeriodDtoFour2.setEndTime(characteristics.get(1).getPeriodEndTime());
        groupPeriodDtoFour2.setSubject(groupsInformation.get(8).getSubjectName());
        groupPeriodDtoFour2.setNumberOfStudents(groupsInformation.get(8).getNumberOfStudents());
        groupPeriodDtoFour2.setInstructorName(groupsInformation.get(8).getInstructor());
        groupPeriodDtoFour2.setRoom(105);
        groupPeriodDtosFour.add(groupPeriodDtoFour2);

        groupStudyDtoFour.setDay("THURSDAY");
        groupStudyDtoFour.setGroupPeriodDtos(groupPeriodDtosFour);

        groupStudyDtos.add(groupStudyDtoFour);

        //FRIDAY
        GroupStudyDto groupStudyDtoFive = new GroupStudyDto();

        List<GroupPeriodDto> groupPeriodDtosFive = new ArrayList<>();

        GroupPeriodDto groupPeriodDtoFive1 = new GroupPeriodDto();
        groupPeriodDtoFive1.setPeriod("Period " + (1));
        groupPeriodDtoFive1.setStartTime(characteristics.get(0).getPeriodStartTime());
        groupPeriodDtoFive1.setEndTime(characteristics.get(0).getPeriodEndTime());
        groupPeriodDtoFive1.setSubject(groupsInformation.get(9).getSubjectName());
        groupPeriodDtoFive1.setNumberOfStudents(groupsInformation.get(9).getNumberOfStudents());
        groupPeriodDtoFive1.setInstructorName(groupsInformation.get(9).getInstructor());
        groupPeriodDtoFive1.setRoom(107);
        groupPeriodDtosFive.add(groupPeriodDtoFive1);

        GroupPeriodDto groupPeriodDtoFive2 = new GroupPeriodDto();
        groupPeriodDtoFive2.setPeriod("Period " + (2));
        groupPeriodDtoFive2.setStartTime(characteristics.get(1).getPeriodStartTime());
        groupPeriodDtoFive2.setEndTime(characteristics.get(1).getPeriodEndTime());
        groupPeriodDtoFive2.setSubject(groupsInformation.get(10).getSubjectName());
        groupPeriodDtoFive2.setNumberOfStudents(groupsInformation.get(10).getNumberOfStudents());
        groupPeriodDtoFive2.setInstructorName(groupsInformation.get(10).getInstructor());
        groupPeriodDtoFive2.setRoom(108);
        groupPeriodDtosFive.add(groupPeriodDtoFive2);

        groupStudyDtoFive.setDay("FRIDAY");
        groupStudyDtoFive.setGroupPeriodDtos(groupPeriodDtosFive);

        groupStudyDtos.add(groupStudyDtoFive);

        groupTable.setGroup(groupsInformation.get(0).getGroup());
        groupTable.setGroupStudyDtoList(groupStudyDtos);

        groupTables.add(groupTable);

        return groupTables;
    }
}
