package time_table.util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DateFormatter {

    public final static String DATE_FORMAT = "dd.MM.yyyy";
    public final static String DATE_TIME_FORMAT = "dd.MM.yyyy HH:mm";

    public static LocalDateTime currentLocalDateTime() {
        return LocalDateTime.now();
    }

    public static LocalDate currentLocalDate() {
        return LocalDate.now();
    }

    public static LocalDate getLocalDate(String rawDate) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMAT);
        return LocalDate.parse(rawDate, formatter);
    }

    public static LocalDateTime getLocalDateTime(String rawDate) {
        if (rawDate == null) return null;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_TIME_FORMAT);
        return LocalDateTime.parse(rawDate, formatter);
    }

    public static String formatLocalDate(LocalDate rawDate) {
        if (rawDate == null) return null;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMAT);
        return rawDate.format(formatter);
    }

    public static String formatLocalDateTime(LocalDateTime rawDate) {
        if (rawDate == null) return null;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_TIME_FORMAT);
        return rawDate.format(formatter);
    }

}
