# TimeTable-Schedule Generator 

The timetable schedule generator in this project uses a genetic algorithm that comes from the source project [Time-table-scheduler](https://github.com/pranavkhurana/Time-table-scheduler).

In this version of the schedule generator, further improvements are done, such as room capacity consideration in regard to the number of students in a group. Furthermore, the work in this project also considers groups and subgroups, where a subject for a group should be taught to all subgroups. For example, a group could be named `BS-Year1` (First year Bachelor's group), and the subgroups of this group could be named `B22-CS-01` (First year bachelor in computer science, subgroup one) and `B22-CS-02` (First year bachelor in computer science, subgroup two)

Room capacity analysis works in such a way that a free room is selected that has at least a capacity equal or slightly superior to the required number of students in a group of subgroup. This is the concept of the smallest room capacity possible that can satisfy the number of students requirement.

## Input

The input data is taken from the [teaching-allocation-v.2.xlsx](teaching-allocation-v.2.xlsx) Excel file of this project.

## Output

A response example that this generator creates can be found in the file [response_example.json](response_example.json)

## Prerequisites

- Java 17 or newer
- Maven, Spring Boot
- RabbitMQ server

## Getting Started

Instructions to use this project:

1. Clone this repository.
2. Import the project into IntelliJ IDEA.
3. Build and run it.

To run this project using its image configured in the [Dockerfile](Dockerfile):

1. Install Docker desktop.
2. Build and run the application with the [docker-compose.yml](docker-compose.yml) file using the command: `docker-compose up`

Once running, the application should work locally under the following link: [http://localhost:8082/swagger-ui/index.html#/](http://localhost:8082/swagger-ui/index.html#/)

The endpoint [http://localhost:8082/schedule/generate](http://localhost:8082/schedule/generate) runs the algorithm and generates the table schedule.

RabbitMQ should work locally under the link [http://localhost:15672/#/queues](http://localhost:15672/#/queues).

## Author

Roberto Enrique Chavez Rodriguez<br>
[r.chavez@innopolis.ru](mailto:r.chavez@innopolis.ru)