FROM maven:3.8.3-openjdk as build
WORKDIR /data
COPY . /data

RUN mvn clean package -DskipTests=true

FROM openjdk:20-ea-4-jdk AS final
WORKDIR /data

COPY --from=build /data/target/TimeTable-generator-sch-0.0.1-SNAPSHOT.jar /data/TimeTable-generator-sch-0.0.1-SNAPSHOT.jar
COPY rabbitmq.properties /data/rabbitmq.properties
COPY teaching-allocation-v.2.xlsx /data/teaching-allocation-v.2.xlsx

EXPOSE 8082
CMD ["java", "-XX:+UseParallelGC", "-Xmx5g", "-jar", "/data/TimeTable-generator-sch-0.0.1-SNAPSHOT.jar"]